﻿using BSTO.Model;
using LiveCharts;
using LiveCharts.Wpf;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using SautinSoft.Document;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Paragraph = MigraDoc.DocumentObjectModel.Paragraph;
using Section = MigraDoc.DocumentObjectModel.Section;

namespace BSTO.ViewModel
{
    public class ReportViewModel : INotifyPropertyChanged
    {
        private DBOperations db;
        private string login;
        private Window window;
        public string text;
        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                OnPropertyChanged("Text");
            }
        }
        public int printType;
        public int PrintType
        {
            get { return printType; }
            set
            {
                printType = value;
                OnPropertyChanged("PrintType");
            }
        }
        public DateTime Ending { get; set; }
        public List<string> Labels { get; set; }
        public ObservableCollection<RouteModel> AllRoutes { get; set; }
        public ObservableCollection<CruiseModel> AllCruises { get; set; }
        public Func<double, string> Formatter { get; set; }

        private SeriesCollection seriesCollection;
        public SeriesCollection SeriesCollection
        {
            get { return seriesCollection; }
            set
            {
                seriesCollection = value;
                OnPropertyChanged("SeriesCollection");
            }
        }
        
        private DateTime calendarDate;
        public DateTime CalendarDate
        {
            get { return calendarDate; }
            set
            {
                calendarDate = value;
                Text = "";
                setRoutes();
                OnPropertyChanged("CalendarDate");
            }
        }

        private DateTime startDate;
        public DateTime StartDate
        {
            get { return startDate; }
            set
            {
                startDate = value;
                OnPropertyChanged("StartDate");
            }
        }

        private DateTime endDate;
        public DateTime EndDate
        {
            get { return endDate; }
            set
            {
                endDate = value;
                OnPropertyChanged("EndDate");
            }
        }

        private int index;
        public int Index
        {
            get { return index; }
            set
            {
                index = value;
                OnPropertyChanged("Index");
            }
        }

        private void setRoutes()
        {
            AllRoutes.Clear();
            if (CalendarDate.Date < DateTime.Now.Date)
            {
                AllRoutes.Add(new RouteModel());
                var list = db.GetRoutesForDate(CalendarDate);
                foreach (RouteModel r in list)
                {
                    AllRoutes.Add(r);
                }
            }
        }

        public RelayCommand Close { get; set; }
        public RelayCommand Summ { get; set; }
        public RelayCommand Show { get; set; }
        public RelayCommand Print { get; set; }

        public ReportViewModel(string login, Window window)
        {
            AllRoutes = new ObservableCollection<RouteModel>();
            this.login = login;
            this.window = window;
            Formatter = value => value.ToString();
            Ending = DateTime.Now.AddDays(-1);
            db = new DBOperations();
            Labels = new List<string>();
            StartDate = Ending;
            EndDate = Ending;
            CalendarDate = Ending.AddDays(-1);
            Text = "";
            AllCruises = new ObservableCollection<CruiseModel>();
            PrintType = 0;
            setCommands();
        }

        private void setCommands()
        {
            setCloseCommand();
            setSummCommand();
            setShowCommand();
            setPrintCommand();
        }

        private void setPrintCommand()
        {
            Print = new RelayCommand(obj =>
            {
                string file = "Отчет " + DateTime.Now.ToString().Replace(" ", "").Replace(".", "").Replace(":", "");
                if (PrintType == 0)
                {
                    Document document = new Document();
                    Section section = document.AddSection();
                    Paragraph main = new Paragraph();
                    section.Add(main);
                    main.AddText(text);
                    PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(true);
                    pdfRenderer.Document = document;
                    pdfRenderer.RenderDocument();
                    pdfRenderer.PdfDocument.Save(file + ".pdf");
                }
                else
                {
                    FileInfo filertf = new FileInfo(file + ".rtf");
                    DocumentCore rtf = new DocumentCore();
                    rtf.Content.End.Insert(text);
                    rtf.Save(filertf.FullName, SaveOptions.RtfDefault);
                }
            }, func => { return text != ""; });
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                Window window = new AdminMain(login);
                window.WindowState = this.window.WindowState;
                window.Top = this.window.Top;
                window.Left = this.window.Left;
                window.Height = this.window.Height;
                window.Width = this.window.Width;
                window.Show();
                this.window.Close();
            });
        }
        private void setSummCommand()
        {
            Summ = new RelayCommand(obj =>
            {
                ChartValues<int> values = new ChartValues<int>();
                Labels.Clear();
                for (DateTime current = startDate.Date; current <= endDate; current = current.AddDays(1))
                {
                    var buffer = db.getTicketsByDate(current);
                    int cost = 0;
                    foreach(TicketModel t in buffer)
                    {
                        cost += t.Cost;
                    }
                    Labels.Add(current.ToString().Substring(0, 10));
                    values.Add(cost);
                }
                values.Add(0);
                SeriesCollection = new SeriesCollection
                {
                new ColumnSeries
                {
                    Title = "",
                    Values = values
                }
                };
            }, func => { return startDate <= endDate; });
        }
        private void setShowCommand()
        {
            Show = new RelayCommand(obj =>
            {
                text = "";
                List<RouteModel> routes = new List<RouteModel>();
                if (Index == 0)
                {
                    routes.AddRange(AllRoutes);
                    routes.Remove(AllRoutes[0]);
                }
                else
                {
                    routes.Add(AllRoutes[Index]);
                }
                text += "Отчет по билетам на: " + CalendarDate.ToString().Substring(0, 10) + '\n';
                text += "Запрошен: " + db.getUserByLogin(login).FIO + '\n';
                foreach (RouteModel route in routes)
                {
                    var routeHalts = db.getRouteHaltsByRouteId(true, route.Number);
                    int cost = 0;
                    text += '\n' + "Маршрут: " + route.Number + '\n';
                    foreach (CruiseModel cruise in route.Cruises)
                    {
                        int[] mask = new int[routeHalts.Count];
                        cost = 0;
                        var tickets = db.getTicketsForCruise(cruise.cruise_id);
                        tickets = tickets.Where(i => i.closed == false && i.date.Date == CalendarDate.Date).ToList();
                        foreach (TicketModel ticket in tickets)
                        {
                            cost += ticket.Cost;
                            if (ticket.returned == false)
                            {
                                int start = routeHalts.Where(i => i.RouteHaltId == ticket.start_halt_id).First().Number;
                                int end = routeHalts.Where(i => i.RouteHaltId == ticket.end_halt_id).First().Number;
                                for (int j = start; j <= end; j++)
                                {
                                    mask[j]++;
                                }
                            }
                        }
                        if (tickets.Count > 0)
                        {
                            Text += "Рейс: " + cruise.cruise_id + '\n';
                            Text += "Время отправки: " + cruise.Time + '\n';
                            Text += "Всего мест: " + db.getTransportById(tickets.First().transport_id).places_count + '\n';
                            Text += "Загруженность по маршруту:" + '\n';
                            for (int i = 1; i < mask.Length; i++)
                            {
                                text += db.getRouteHaltAdressLocality(routeHalts[i - 1].RouteHaltId) + " - " + db.getRouteHaltAdressLocality(routeHalts[i].RouteHaltId) + ": " + mask[i] + '\n';
                            }
                            Text += "Общая прибыль: " + cost + '\n';
                        }
                    }
                }
            }, func => { return Index >= 0; });
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
