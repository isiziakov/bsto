﻿using BSTO.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BSTO.ViewModel
{
    public class TransportViewModel : INotifyPropertyChanged
    {
        private DBOperations db;
        private string login;
        private Bus transportWindow;

        private bool showHidden = false;
        public bool ShowHidden
        {
            get { return showHidden; }
            set
            {
                showHidden = value;
                setAllTransports();
                OnPropertyChanged("ShowHidden");
            }
        }
        private ObservableCollection<TransportModel> allTransports;
        public ObservableCollection<TransportModel> AllTransports
        {
            get { return allTransports; }
            set
            {
                allTransports = value;
                OnPropertyChanged("AllTransports");
            }
        }

        private TransportModel selectedTransport;
        public TransportModel SelectedTransport
        {
            get { return selectedTransport; }
            set
            {
                selectedTransport = value;
                if (selectedTransport != null)
                {
                    CurrentNumber = selectedTransport.Number;
                    CurrentPlaces = selectedTransport.Places_count.ToString();
                }
                OnPropertyChanged("SelectedTransport");
            }
        }

        private string searchingContext;
        public string SearchingContext
        {
            get { return searchingContext; }
            set
            {
                searchingContext = value;
                setAllTransports();
                OnPropertyChanged("SearchingContext");
            }
        }

        private string currentNumber;
        public string CurrentNumber
        {
            get { return currentNumber; }
            set
            {
                currentNumber = value;
                OnPropertyChanged("CurrentNumber");
            }
        }

        private string currentPlaces;
        public string CurrentPlaces
        {
            get { return currentPlaces; }
            set
            {
                currentPlaces = value;
                OnPropertyChanged("CurrentPlaces");
            }
        }

        public RelayCommand Close { get; set; }

        public RelayCommand Add { get; set; }

        public RelayCommand Update { get; set; }

        public RelayCommand HideShow { get; set; }

        public TransportViewModel(string login, Bus transportWindow)
        {
            this.login = login;
            this.transportWindow = transportWindow;
            searchingContext = "";
            db = new DBOperations();
            setCommands();
            setAllTransports();
        }

        private void setCommands()
        {
            setCloseCommand();
            setAddCommand();
            setUpdateCommand();
            setHideShowCommand();
        }

        private void setHideShowCommand()
        {
            HideShow = new RelayCommand(obj =>
            {
                SelectedTransport.Hidden = (SelectedTransport.Hidden + 1) % 2;
                db.updateTransport(selectedTransport);
                if (showHidden == false && SelectedTransport.Hidden == 1)
                {
                    AllTransports.Remove(SelectedTransport);
                }
            },
            obj => { return selectedTransport != null; });
        }
        private void setUpdateCommand()
        {
            Update = new RelayCommand(obj =>
            {
                SelectedTransport.Hidden = 2;
                db.updateTransport(selectedTransport);
                TransportModel transport = new TransportModel();
                transport.Number = currentNumber;
                transport.Places_count = Int32.Parse(currentPlaces);
                transport.Hidden = 0;
                transport.transport_id = db.addTransport(transport);
                AllTransports.Insert(0, transport);
                db.updateCruisesTransport(selectedTransport.transport_id, transport.transport_id);
                AllTransports.Remove(SelectedTransport);
                SelectedTransport = transport;
            },
            obj => { return currentNumber != "" && Int32.TryParse(currentPlaces, out int number) == true && selectedTransport != null; });
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                Window window = new AdminMain(login);
                window.WindowState = transportWindow.WindowState;
                window.Top = transportWindow.Top;
                window.Left = transportWindow.Left;
                window.Height = transportWindow.Height;
                window.Width = transportWindow.Width;
                window.Show();
                transportWindow.Close();
            });
        }

        private void setAddCommand()
        {
            Add = new RelayCommand(obj =>
            {
                TransportModel transport = new TransportModel();
                transport.Number = currentNumber;
                transport.Places_count = Int32.Parse(currentPlaces);
                transport.Hidden = 0;
                transport.transport_id = db.addTransport(transport);
                AllTransports.Insert(0, transport);
                SelectedTransport = transport;
            },
            obj => { return currentNumber != "" && Int32.TryParse(currentPlaces, out int number) == true; });
        }

        private void setAllTransports()
        {
            List<TransportModel> transportList = db.getTransportsWithContext(showHidden, searchingContext);
            if (transportList == null)
            {
                if (AllTransports == null)
                {
                    AllTransports = new ObservableCollection<TransportModel>();
                }
            }
            else
            {
                AllTransports = new ObservableCollection<TransportModel>(transportList);
            }
            CurrentNumber = "";
            CurrentPlaces = "";
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
