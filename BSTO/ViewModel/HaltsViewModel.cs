﻿using BSTO.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BSTO.ViewModel
{
    public class HaltsViewModel : INotifyPropertyChanged
    {
        private DBOperations db;
        private string login;
        private Halts haltsWindow;
        
        private bool showHidden = false;
        public bool ShowHidden
        {
            get { return showHidden; }
            set
            {
                showHidden = value;
                setAllHalts();
                OnPropertyChanged("ShowHidden");
            }
        }
        public List<LocalityModel> Localities { get; set; }

        private ObservableCollection<HaltModel> allHalts;
        public ObservableCollection<HaltModel> AllHalts
        {
            get { return allHalts; }
            set
            {
                allHalts = value;
                OnPropertyChanged("AllHalts");
            }
        }

        private HaltModel selectedHalt;
        public HaltModel SelectedHalt
        {
            get { return selectedHalt; }
            set
            {
                selectedHalt = value;
                if (selectedHalt != null)
                {
                    CurrentAdress = selectedHalt.Adress;
                    CurrentLocasity = Localities.Where(i => i.locality_id == selectedHalt.Locality_model.locality_id).FirstOrDefault();
                }
                OnPropertyChanged("SelectedHalt");
            }
        }

        private string searchingContext;
        public string SearchingContext
        {
            get { return searchingContext; }
            set
            {
                searchingContext = value;
                setAllHalts();
                OnPropertyChanged("SearchingContext");
            }
        }

        private string currentAdress;
        public string CurrentAdress
        {
            get { return currentAdress; }
            set
            {
                currentAdress = value;
                OnPropertyChanged("CurrentAdress");
            }
        }

        private LocalityModel currentLocasity;
        public LocalityModel CurrentLocasity
        {
            get { return currentLocasity; }
            set
            {
                currentLocasity = value;
                OnPropertyChanged("CurrentLocasity");
            }
        }

        public RelayCommand Close { get; set; }

        public RelayCommand Add { get; set; }

        public RelayCommand Update { get; set; }

        public RelayCommand HideShow { get; set; }

        public HaltsViewModel(string login, Halts haltsWindow)
        {
            this.login = login;
            this.haltsWindow = haltsWindow;
            searchingContext = "";
            db = new DBOperations();
            setCommands();
            setAllHalts();
        }

        private void setCommands()
        {
            setCloseCommand();
            setAddCommand();
            setUpdateCommand();
            setHideShowCommand();
        }

        private void setHideShowCommand()
        {
            HideShow = new RelayCommand(obj =>
            {
                SelectedHalt.Hidden = (SelectedHalt.Hidden + 1) % 2;
                db.updateHalt(selectedHalt);
                if (showHidden == false && SelectedHalt.Hidden == 1)
                {
                    AllHalts.Remove(SelectedHalt);
                }
            },
            obj => { return selectedHalt != null; });
        }
        private void setUpdateCommand()
        {
            Update = new RelayCommand(obj =>
            {
                SelectedHalt.Adress = currentAdress;
                SelectedHalt.Locality_model = currentLocasity;
                SelectedHalt.locality_id = currentLocasity.locality_id;
                db.updateHalt(selectedHalt);
            },
            obj => { return currentAdress != "" && currentLocasity != null && selectedHalt != null; });
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                Window window = new AdminMain(login);
                window.WindowState = haltsWindow.WindowState;
                window.Top = haltsWindow.Top;
                window.Left = haltsWindow.Left;
                window.Height = haltsWindow.Height;
                window.Width = haltsWindow.Width;
                window.Show();
                haltsWindow.Close();
            });
        }

        private void setAddCommand()
        {
            Add = new RelayCommand(obj =>
            {
                HaltModel halt = new HaltModel();
                halt.Adress = currentAdress;
                halt.Locality_model = currentLocasity;
                halt.locality_id = currentLocasity.locality_id;
                halt.Hidden = 0;
                halt.halt_id = db.addHalt(halt);
                AllHalts.Insert(0, halt);
                SelectedHalt = halt;
            },
            obj => { return currentAdress != "" && currentLocasity != null; });
        }

        private void setAllHalts()
        {
            List<HaltModel> haltsList = db.getHaltsWithContext(showHidden, searchingContext);
            if (haltsList == null)
            {
                if (AllHalts == null)
                {
                    AllHalts = new ObservableCollection<HaltModel>();
                }
            }
            else
            {
                AllHalts = new ObservableCollection<HaltModel>(haltsList);
            }
            Localities = db.getLocalities();
            CurrentLocasity = null;
            CurrentAdress = "";
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
