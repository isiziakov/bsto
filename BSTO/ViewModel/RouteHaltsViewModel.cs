﻿using BSTO.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BSTO.ViewModel
{
    public class RouteHaltsViewModel : INotifyPropertyChanged
    {
        private DBOperations db;
        private string login;
        private Window currentWindow;

        private DateTime selectedDate;
        public DateTime SelectedDate
        {
            get { return selectedDate; }
            set
            {
                selectedDate = value;
                OnPropertyChanged("SelectedDate");
            }
        }
        public DateTime StartDate { get; set; }

        public List<LocalityModel> Localities { get; set; }
        public List<RouteModel> Routes { get; set; }

        private bool isExport;
        public bool IsExport
        {
            get { return isExport; }
            set
            {
                isExport = value;
                OnPropertyChanged("IsExport");
            }
        }

        private RouteModel selectedRoute;
        public RouteModel SelectedRoute
        {
            get { return selectedRoute; }
            set
            {
                selectedRoute = value;
                OnPropertyChanged("SelectedRoute");
            }
        }

        private LocalityModel currentLocasity;
        public LocalityModel CurrentLocasity
        {
            get { return currentLocasity; }
            set
            {
                currentLocasity = value;
                setAllHalts();
                OnPropertyChanged("CurrentLocasity");
            }
        }

        private ObservableCollection<HaltModel> allHalts;
        public ObservableCollection<HaltModel> AllHalts
        {
            get { return allHalts; }
            set
            {
                allHalts = value;
                OnPropertyChanged("AllHalts");
            }
        }

        private HaltModel selectedHalt;
        public HaltModel SelectedHalt
        {
            get { return selectedHalt; }
            set
            {
                selectedHalt = value;
                OnPropertyChanged("SelectedHalt");
            }
        }

        private ObservableCollection<RouteHaltModel> allRouteHalts;
        public ObservableCollection<RouteHaltModel> AllRouteHalts
        {
            get { return allRouteHalts; }
            set
            {
                allRouteHalts = value;
                OnPropertyChanged("AllRouteHalts");
            }
        }

        private RouteHaltModel selectedRouteHalt;
        public RouteHaltModel SelectedRouteHalt
        {
            get { return selectedRouteHalt; }
            set
            {
                selectedRouteHalt = value;
                if (selectedRouteHalt != null)
                {
                    CurrentLocasity = Localities.Find(i => i.locality_name == selectedRouteHalt.Locality);
                    SelectedHalt = AllHalts.Where(i => i.halt_id == selectedRouteHalt.HaltId).FirstOrDefault();
                    CurrentCost = selectedRouteHalt.Cost.ToString();
                    CurrentTime = selectedRouteHalt.Time.ToString();
                }
                OnPropertyChanged("SelectedRouteHalt");
            }
        }

        private string currentTime;
        public string CurrentTime
        {
            get { return currentTime; }
            set
            {
                currentTime = value;
                OnPropertyChanged("CurrentTime");
            }
        }

        private string currentCost;
        public string CurrentCost
        {
            get { return currentCost; }
            set
            {
                currentCost = value;
                OnPropertyChanged("CurrentCost");
            }
        }

        private void setAllHalts()
        {
            if (CurrentLocasity != null)
            {
                AllHalts = new ObservableCollection<HaltModel>(db.getHaltsByLocality(CurrentLocasity.locality_id));
                if (AllHalts.Count > 0)
                {
                    if (selectedRouteHalt != null && selectedRouteHalt.Locality == currentLocasity.locality_name)
                    {
                        SelectedHalt = AllHalts.Where(i => i.halt_id == selectedRouteHalt.HaltId).FirstOrDefault();
                    }
                    else
                    {
                        SelectedHalt = null;
                    }
                }
            }
        }

        public RelayCommand Close { get; set; }

        public RelayCommand Add { get; set; }

        public RelayCommand Update { get; set; }

        public RelayCommand HideShow { get; set; }

        public RelayCommand Up { get; set; }

        public RelayCommand Down { get; set; }

        public RelayCommand Save { get; set; }

        public RouteHaltsViewModel(string login, Window window)
        {
            db = new DBOperations();
            allRouteHalts = new ObservableCollection<RouteHaltModel>();
            IsExport = false;
            StartDate = DateTime.Now.AddDays(1);
            SelectedDate = StartDate;
            Localities = db.getLocalities();
            Routes = db.getRoutes(false);
            CurrentLocasity = null;
            SelectedRouteHalt = null;
            this.login = login;
            currentWindow = window;
            setCommands();
        }

        private void setCommands()
        {
            setCloseCommand();
            setAddCommand();
            setUpdateCommand();
            setHideShowCommand();
            setUpCommand();
            setDownCommand();
            setSaveCommand();
        }

        private void setSaveCommand()
        {
            Save = new RelayCommand(obj =>
            {
                RouteModel route = new RouteModel();
                route.Hidden = 0;
                route.Number = db.addRoute(route);
                AllRouteHalts.First().Time = 0;
                AllRouteHalts.First().Cost = 0;
                foreach (RouteHaltModel halt in AllRouteHalts)
                {
                    halt.RouteId = route.Number;
                    halt.RouteHaltId = db.addRouteHalt(halt);
                }
                if (IsExport && SelectedRoute != null)
                {
                    List<CruiseModel> cruises = db.getCruisesForRoute(SelectedRoute.Number, false);
                    foreach (CruiseModel cr in cruises)
                    {
                        CruiseModel buffer = new CruiseModel();
                        buffer.day = cr.day;
                        buffer.driver_id = cr.driver_id;
                        buffer.EndingDate = cr.EndingDate;
                        buffer.first = SelectedDate;
                        buffer.StartDate = SelectedDate.Date.ToString().Substring(0, 10);
                        buffer.route_id = route.Number;
                        buffer.Time = cr.Time;
                        buffer.transport_id = cr.transport_id;
                        buffer.Hidden = cr.Hidden;
                        cr.EndingDate = SelectedDate.AddDays(-1).ToString().Substring(0, 10);
                        db.updateCruise(cr);
                        buffer.cruise_id = db.addCruise(buffer);

                        List<TicketModel> tickets = db.getTicketsForCruise(cr.cruise_id).Where(i => i.date >= SelectedDate.Date).ToList();
                        foreach (TicketModel t in tickets)
                        {
                            t.route_id = route.Number;
                            t.cruise_id = cr.cruise_id;
                            if (AllRouteHalts.Where(i => i.HaltId == db.getRouteHaltHalt(t.start_halt_id)).FirstOrDefault() == null || AllRouteHalts.Where(i => i.HaltId == db.getRouteHaltHalt(t.end_halt_id)).FirstOrDefault() == null)
                            {
                                t.closed = true;
                                
                            }
                            else
                            {
                                t.start_halt_id = AllRouteHalts.Where(i => i.HaltId == db.getRouteHaltHalt(t.start_halt_id)).FirstOrDefault().RouteHaltId;
                                t.end_halt_id = AllRouteHalts.Where(i => i.HaltId == db.getRouteHaltHalt(t.end_halt_id)).FirstOrDefault().RouteHaltId;
                                if (t.date.AddMinutes(AllRouteHalts.Where(i => i.HaltId == db.getRouteHaltHalt(t.start_halt_id)).FirstOrDefault().Time) != t.start_date)
                                {
                                    t.rtime = true;
                                    t.start_date = t.date.AddMinutes(AllRouteHalts.Where(i => i.HaltId == db.getRouteHaltHalt(t.start_halt_id)).FirstOrDefault().Time);
                                }
                            }
                            db.updateTicket(t);
                        }
                    }
                }
                CloseThis();
            },
            obj => { return AllRouteHalts.Count > 0; });
        }

        private void setUpCommand()
        {
            Up = new RelayCommand(obj =>
            {
                int buf = AllRouteHalts.IndexOf(selectedRouteHalt);
                AllRouteHalts[buf].Number--;
                AllRouteHalts[buf - 1].Number++;
                RouteHaltModel buffer = AllRouteHalts[buf];
                AllRouteHalts[buf] = AllRouteHalts[buf - 1];
                AllRouteHalts[buf - 1] = buffer;
            },
            obj => { return selectedRouteHalt != null && selectedRouteHalt.Number > 0; });
        }
        private void setDownCommand()
        {
            Down = new RelayCommand(obj =>
            {
                int buf = AllRouteHalts.IndexOf(selectedRouteHalt);
                AllRouteHalts[buf].Number++;
                AllRouteHalts[buf + 1].Number--;
                RouteHaltModel buffer = AllRouteHalts[buf];
                AllRouteHalts[buf] = AllRouteHalts[buf + 1];
                AllRouteHalts[buf + 1] = buffer;
            },
            obj => { return selectedRouteHalt != null && selectedRouteHalt.Number < AllRouteHalts.Count - 1; });
        }
        private void setHideShowCommand()
        {
            HideShow = new RelayCommand(obj =>
            {
                foreach (RouteHaltModel r in AllRouteHalts)
                {
                    if (r.Number > selectedRouteHalt.Number)
                    {
                        r.Number--;
                    }
                }
                AllRouteHalts.Remove(selectedRouteHalt);
            },
            obj => { return selectedRouteHalt != null; });
        }
        private void setUpdateCommand()
        {
            Update = new RelayCommand(obj =>
            {
                SelectedRouteHalt.HaltId = selectedHalt.halt_id;
                SelectedRouteHalt.Cost = int.Parse(currentCost);
                SelectedRouteHalt.Time = int.Parse(currentTime);
                SelectedRouteHalt.Locality = CurrentLocasity.locality_name;
                SelectedRouteHalt.Adress = selectedHalt.Adress;
            },
            obj => { return currentLocasity != null && int.TryParse(currentCost, out int number) && int.Parse(currentCost) >= 0 && int.TryParse(currentTime, out int number2) && int.Parse(currentTime) >= 0 &&  selectedHalt != null && selectedRouteHalt != null; });
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                CloseThis();
            });
        }

        private void CloseThis()
        {
            Window window = new Route(login);
            window.WindowState = currentWindow.WindowState;
            window.Top = currentWindow.Top;
            window.Left = currentWindow.Left;
            window.Height = currentWindow.Height;
            window.Width = currentWindow.Width;
            window.Show();
            currentWindow.Close();
        }

        private void setAddCommand()
        {
            Add = new RelayCommand(obj =>
            {
                RouteHaltModel routeHalt = new RouteHaltModel();
                routeHalt.HaltId = selectedHalt.halt_id;
                routeHalt.Hidden = 0;
                routeHalt.Cost = int.Parse(currentCost);
                routeHalt.Time = int.Parse(currentTime);
                routeHalt.Number = AllRouteHalts.Count;
                routeHalt.Locality = CurrentLocasity.locality_name;
                routeHalt.Adress = selectedHalt.Adress;
                AllRouteHalts.Add(routeHalt);
            },
            obj => { return CurrentLocasity != null && int.TryParse(currentTime, out int number0) && int.Parse(currentTime) >= 0  && int.TryParse(currentCost, out int number) && int.Parse(currentCost) >= 0 && selectedHalt != null; });
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
