﻿using BSTO.Model;
using BSTO.View;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BSTO.ViewModel
{
    public class RoutesViewModel : INotifyPropertyChanged
    {
        private DBOperations db;
        private string login;
        private Route routesWindow;

        private bool showHidden = false;
        public bool ShowHidden
        {
            get { return showHidden; }
            set
            {
                showHidden = value;
                setAllRoutes();
                OnPropertyChanged("ShowHidden");
            }
        }

        private bool showDrivers = false;
        public bool ShowDrivers
        {
            get { return showDrivers; }
            set
            {
                showDrivers = value;
                setAllRoutes();
                OnPropertyChanged("ShowDrivers");
            }
        }

        private bool showTransports = false;
        public bool ShowTransports
        {
            get { return showTransports; }
            set
            {
                showTransports = value;
                setAllRoutes();
                OnPropertyChanged("ShowTransports");
            }
        }

        public ObservableCollection<RouteModel> AllRoutes
        {
            get;
            set;
        }

        private RouteModel selectedRoute;
        public RouteModel SelectedRoute
        {
            get { return selectedRoute; }
            set
            {
                selectedRoute = value;
                if (selectedRoute != null)
                {
                    CurrentNumber = selectedRoute.Number.ToString();
                }
                OnPropertyChanged("SelectedRoute");
            }
        }

        private string searchingContext;
        public string SearchingContext
        {
            get { return searchingContext; }
            set
            {
                searchingContext = value;
                setAllRoutes();
                OnPropertyChanged("SearchingContext");
            }
        }

        private string currentNumber;
        public string CurrentNumber
        {
            get { return currentNumber; }
            set
            {
                currentNumber = value;
                OnPropertyChanged("CurrentNumber");
            }
        }

        public RelayCommand Export { get; set; }
        public RelayCommand Close { get; set; }

        public RelayCommand Update { get; set; }

        public RelayCommand HideShow { get; set; }

        public RelayCommand ShowCruises { get; set; }

        public RelayCommand ShowHalts { get; set; }

        public RoutesViewModel(string login, Route routesWindow)
        {
            this.login = login;
            this.routesWindow = routesWindow;
            searchingContext = "";
            AllRoutes = new ObservableCollection<RouteModel>();
            db = new DBOperations();
            setCommands();
            setAllRoutes();
        }

        private void setCommands()
        {
            setCloseCommand();
            setUpdateCommand();
            setHideShowCommand();
            setCruisesShowCommand();
            setExportCommand();
            setHaltsShowCommand();
        }

        private void setExportCommand()
        {
            Export = new RelayCommand(obj =>
            {
                ExcelPackage excel = new ExcelPackage();
                var workSheet = excel.Workbook.Worksheets.Add("Расписание процедурных");
                workSheet.DefaultRowHeight = 80;
                workSheet.DefaultColWidth = 28;
                workSheet.Cells.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                workSheet.Cells.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                int counter = 1;
                foreach (RouteModel route in AllRoutes)
                {
                    if (route.Hidden == 0)
                    {
                       if (route.GroupCruises != null && route.GroupCruises.Count > 0)
                        {
                            workSheet.Cells[counter, 1].Value = route.Number.ToString() + "\n" + route.start + "\n" + route.ending;
                            for (int i = 0; i < route.GroupCruises.Count; i++)
                            {
                                string text = "";
                                text = route.GroupCruises[i].daysStr + "\n" + route.GroupCruises[i].cruises[0].Time;
                                if (route.GroupCruises[i].cruises[0].StartDate != "" && DateTime.Parse(route.GroupCruises[i].cruises[0].StartDate).Date >= DateTime.Now.Date)
                                {
                                    text += "\n" + "С" + "\n" + route.GroupCruises[i].cruises[0].StartDate;
                                }
                                if (route.GroupCruises[i].cruises[0].EndingDate != "" && DateTime.Parse(route.GroupCruises[i].cruises[0].EndingDate).Date >= DateTime.Now.Date)
                                {
                                    text += "\n" + "До" + "\n" + route.GroupCruises[i].cruises[0].EndingDate;
                                }
                                workSheet.Cells[counter, i + 2].Value = text;
                            }
                            counter++;
                        }
                    }
                }
                string p_strPath = "Расписание" + DateTime.Now.ToString().Replace(" ", "").Replace(".", "").Replace(":", "") + ".xlsx";
                FileStream objFileStrm = File.Create(p_strPath);
                objFileStrm.Close();
                File.WriteAllBytes(p_strPath, excel.GetAsByteArray());
            },
            obj => { return AllRoutes.Count > 0; });
        }

        private void setCruisesShowCommand()
        {
            ShowCruises = new RelayCommand(obj =>
            {
                OpenWindow(new Cruise(login, SelectedRoute.Number));
            },
            obj => { return selectedRoute != null; });
        }

        private void setHaltsShowCommand()
        {
            ShowHalts = new RelayCommand(obj =>
            {
                OpenWindow(new RouteHalts(login));
            });
        }

        public void OpenWindow(Window newWindow)
        {
            newWindow.WindowState = routesWindow.WindowState;
            newWindow.Top = routesWindow.Top;
            newWindow.Left = routesWindow.Left;
            newWindow.Height = routesWindow.Height;
            newWindow.Width = routesWindow.Width;
            newWindow.Show();
            routesWindow.Close();
        }

        private void setHideShowCommand()
        {
            HideShow = new RelayCommand(obj =>
            {
                SelectedRoute.Hidden = (SelectedRoute.Hidden + 1) % 2;
                db.updateRoute(SelectedRoute);
                if (showHidden == false && SelectedRoute.Hidden == 1)
                {
                    AllRoutes.Remove(SelectedRoute);
                }
            },
            obj => { return selectedRoute != null; });
        }
        private void setUpdateCommand()
        {
            Update = new RelayCommand(obj =>
            {
                OpenWindow(new EditRouteHalt(login, SelectedRoute.Number));
            },
            obj =>
            {
                return selectedRoute != null;
            });
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                Window window = new AdminMain(login);
                window.WindowState = routesWindow.WindowState;
                window.Top = routesWindow.Top;
                window.Left = routesWindow.Left;
                window.Height = routesWindow.Height;
                window.Width = routesWindow.Width;
                window.Show();
                routesWindow.Close();
            });
        }

        private void setAllRoutes()
        {
            List<RouteModel> routeList = db.getRoutesWithContext(showHidden, searchingContext);
            AllRoutes.Clear();
            foreach (RouteModel r in routeList)
            {
                List<CruiseModel> cruises = db.getCruisesForRoute(r.Number, false);
                cruises = cruises.Where(i => i.EndingDate == "" || DateTime.Parse(i.EndingDate) >= DateTime.Now.Date).ToList();
                r.Cruises = new ObservableCollection<CruiseModel>();
                AllRoutes.Add(r);
                if (cruises.Count > 0)
                {
                    r.Cruises = new ObservableCollection<CruiseModel>(cruises);
                    List<CruisesModel> buffer = new List<CruisesModel>();
                    foreach (CruiseModel cruise in r.Cruises)
                    {
                        bool flag = false;
                        foreach (CruisesModel cr in buffer)
                        {
                            if (cr.RouteComparer(cruise))
                            {
                                cr.Add(cruise);
                                flag = true;
                                break;
                            }
                        }
                        if (!flag)
                        {
                            buffer.Add(new CruisesModel(cruise));
                        }
                    }
                    r.GroupCruises = new ObservableCollection<CruisesModel>(buffer.OrderBy(x => x.cruises.First().Time));
                }
                else
                {
                    r.Cruises = new ObservableCollection<CruiseModel>();
                }
            }
            CurrentNumber = "";
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
