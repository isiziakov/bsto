﻿using BSTO.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BSTO.ViewModel
{
    public class CruiseViewModel : INotifyPropertyChanged
    {
        private int routeId;
        private DBOperations db;
        private string login;
        private Cruise cruiseWindow;

        private bool showHidden = false;
        public bool ShowHidden
        {
            get { return showHidden; }
            set
            {
                showHidden = value;
                setAllCruises();
                OnPropertyChanged("ShowHidden");
            }
        }
        private ObservableCollection<CruisesModel> allCruises;
        public ObservableCollection<CruisesModel> AllCruises
        {
            get { return allCruises; }
            set
            {
                allCruises = value;
                OnPropertyChanged("AllCruises");
            }
        }

        public ObservableCollection<DriverModel> AllDrivers
        {
            get;
            set;
        }

        public ObservableCollection<TransportModel> AllTransport
        {
            get;
            set;
        }

        private CruisesModel selectedCruise;
        public CruisesModel SelectedCruise
        {
            get { return selectedCruise; }
            set
            {
                selectedCruise = value;
                if (selectedCruise != null && selectedCruise.cruises.Count > 0)
                {
                    CurrentDriver = AllDrivers.Where(i => i.driver_id == SelectedCruise.cruises.First().driver_id).FirstOrDefault();
                    if (CurrentDriver == null)
                    {
                        CurrentDriver = AllDrivers[0];
                    }
                    CurrentTransport = AllTransport.Where(i => i.transport_id == SelectedCruise.cruises.First().transport_id).FirstOrDefault();
                    if (CurrentTransport == null)
                    {
                        CurrentTransport = AllTransport[0];
                    }
                    CurrentTime = selectedCruise.cruises.First().Time;
                    CurrentEnding = selectedCruise.cruises.First().EndingDate;
                    CurrentStart = selectedCruise.cruises.First().StartDate;
                }
                OnPropertyChanged("SelectedCruise");
            }
        }

        public ObservableCollection<bool> Days { get; set; }

        private string currentTime;
        public string CurrentTime
        {
            get { return currentTime; }
            set
            {
                currentTime = value;
                OnPropertyChanged("CurrentTime");
            }
        }

        private string currentEnding;
        public string CurrentEnding
        {
            get { return currentEnding; }
            set
            {
                currentEnding = value;
                OnPropertyChanged("CurrentEnding");
            }
        }

        private string currentStart;
        public string CurrentStart
        {
            get { return currentStart; }
            set
            {
                currentStart = value;
                OnPropertyChanged("CurrentStart");
            }
        }

        private TransportModel currentTransport;
        public TransportModel CurrentTransport
        {
            get { return currentTransport; }
            set
            {
                currentTransport = value;
                OnPropertyChanged("CurrentTransport");
            }
        }

        private DriverModel currentDriver;
        public DriverModel CurrentDriver
        {
            get { return currentDriver; }
            set
            {
                currentDriver = value;
                OnPropertyChanged("CurrentDriver");
            }
        }

        public RelayCommand Close { get; set; }

        public RelayCommand Add { get; set; }

        public RelayCommand Update { get; set; }

        public RelayCommand HideShow { get; set; }
        public RelayCommand Minus { get; set; }
        public RelayCommand Plus { get; set; }

        public CruiseViewModel(string login, Cruise cruiseWindow, int routeId)
        {
            this.login = login;
            this.cruiseWindow = cruiseWindow;
            this.routeId = routeId;
            Days = new ObservableCollection<bool>() { false, false, false, false, false, false, false};
            db = new DBOperations();
            setCommands();
            setAllCruises();
        }

        private void setCommands()
        {
            setCloseCommand();
            setAddCommand();
            setUpdateCommand();
            setHideShowCommand();
            Plus = new RelayCommand(obj =>
            {
                for (int i = 0; i < 7; i++)
                {
                    Days[i] = true;
                }
            });
            Minus = new RelayCommand(obj =>
            {
                for (int i = 0; i < 7; i++)
                {
                    Days[i] = false;
                }
            });
        }

        private bool CheckUpdate()
        {
            if (SelectedCruise == null)
            {
                return false;
            }
            for (int i = 0; i < 7; i++)
            {
                if (SelectedCruise.daysMask[i] == false && Days[i] == true)
                {
                    return false;
                }
            }
            return true;
        }

        private void setHideShowCommand()
        {
            HideShow = new RelayCommand(obj =>
            {
                foreach (CruiseModel cruise in SelectedCruise.cruises)
                {
                    if (Days[cruise.day - 1] == true)
                    {
                        cruise.Hidden = (cruise.Hidden + 1) % 2;
                        db.updateCruise(cruise);
                    }
                }
                setAllCruises();
            },
            obj => { return CheckUpdate(); });
        }
        private void setUpdateCommand()
        {
            Update = new RelayCommand(obj =>
            {
                int time = Convert.ToInt32((TimeSpan.Parse(currentTime) - TimeSpan.Parse(SelectedCruise.cruises.First().Time)).TotalMinutes);
                for (int i = 0; i < 7; i++)
                {
                    if (SelectedCruise.daysMask[i] == Days[i] && Days[i])
                    {
                        CruiseModel currentCruise = SelectedCruise.cruises.Where(j => j.day - 1 == i).FirstOrDefault();
                        CruiseModel buffer = new CruiseModel();
                        buffer.Time = currentTime;
                        buffer.driver_id = currentDriver.driver_id;
                        buffer.day = currentCruise.day;
                        buffer.transport_id = currentTransport.transport_id;
                        buffer.Hidden = SelectedCruise.cruises.First().Hidden;
                        buffer.StartDate = currentCruise.StartDate;
                        buffer.EndingDate = currentCruise.EndingDate;
                        if (CurrentStart == "" || DateTime.Parse(CurrentStart) < DateTime.Now)
                        {
                            buffer.StartDate = "";
                        }
                        else
                        {
                            if (currentCruise.StartDate == null || DateTime.Parse(currentCruise.StartDate) < DateTime.Parse(CurrentStart))
                            {
                                buffer.StartDate = CurrentStart;
                                buffer.first = DateTime.Parse(CurrentStart);
                            }
                        }
                        if (CurrentEnding == "" || DateTime.Parse(CurrentEnding) < DateTime.Now)
                        {
                            buffer.EndingDate = "";
                        }
                        else
                        {
                            if (currentCruise.EndingDate == null || DateTime.Parse(currentCruise.EndingDate) > DateTime.Parse(CurrentEnding))
                            {
                                buffer.EndingDate = CurrentEnding;
                            }
                        }
                        buffer.day = i + 1;
                        bool f = true;
                        foreach (CruisesModel cr in AllCruises)
                        {
                            if (cr.Comparer(buffer))
                            {
                                if (cr.daysMask[buffer.day - 1])
                                {
                                    f = false;
                                    break;
                                }
                            }
                        }
                        if (f)
                        {
                            List<TicketModel> tickets = db.getTicketsForCruise(currentCruise.cruise_id).Where(j => j.date > DateTime.Now).ToList();
                            foreach (TicketModel t in tickets)
                            {
                                bool changing = false;
                                if (time != 0)
                                {
                                    t.date.AddMinutes(time);
                                    t.start_date.AddMinutes(time);
                                    t.rtime = true;
                                    changing = true;
                                }
                                if (currentCruise.driver_id != currentDriver.driver_id)
                                {
                                    t.driver_id = currentDriver.driver_id;
                                    changing = true;
                                }
                                if (currentCruise.transport_id != CurrentTransport.transport_id)
                                {
                                    t.transport_id = CurrentTransport.transport_id;
                                    if (CurrentTransport.Places_count < t.Place)
                                    {
                                        List<int> places = db.getFreePlaces(currentCruise.cruise_id, t.date, t.start_halt_id, t.end_halt_id);
                                        if (places.Count > 0)
                                        {
                                            t.Place = places.First();
                                            t.rplace = true;
                                        }
                                        else
                                        {
                                            t.closed = true;
                                        }
                                    }
                                }
                                if (CurrentStart != "")
                                {
                                    if (DateTime.Parse(CurrentStart) > DateTime.Now)
                                    {
                                        if (currentCruise.StartDate == null || DateTime.Parse(currentCruise.StartDate) < DateTime.Parse(CurrentStart))
                                        {
                                            if (DateTime.Parse(CurrentStart) > t.date)
                                            {
                                                t.closed = true;
                                            }
                                        } 
                                    }
                                }
                                if (CurrentEnding != "")
                                {
                                    if (DateTime.Parse(CurrentEnding) > DateTime.Now)
                                    {
                                        if (currentCruise.EndingDate == null || DateTime.Parse(currentCruise.EndingDate) > DateTime.Parse(CurrentEnding))
                                        {
                                            if (DateTime.Parse(CurrentEnding) < t.date)
                                            {
                                                t.closed = true;
                                            }
                                        }
                                    }
                                }
                                if (changing)
                                {
                                    db.updateTicket(t);
                                }
                            }
                            currentCruise.Time = CurrentTime;
                            currentCruise.driver_id = currentDriver.driver_id;
                            currentCruise.Driver = db.getDriver(currentDriver.driver_id);
                            currentCruise.transport_id = CurrentTransport.transport_id;
                            currentCruise.Transport = db.getTransport(CurrentTransport.transport_id);
                            currentCruise.StartDate = buffer.StartDate;
                            currentCruise.EndingDate = buffer.EndingDate;
                            if (buffer.first != null)
                            {
                                currentCruise.first = buffer.first;
                            }
                            db.updateCruise(currentCruise);
                        }
                    }
                }
                setAllCruises();
            },
            obj => { return SelectedCruise != null && DateTime.TryParse(currentTime, out DateTime number) && CurrentTime.Length == 5 && ((DateTime.TryParse(currentEnding, out DateTime secondNumber) && CurrentEnding.Length == 10) || currentEnding == "") && ((DateTime.TryParse(currentStart, out DateTime thirdNumber) && currentStart.Length == 10) || currentStart == "") && currentDriver != null && currentTransport != null; });
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                Window window = new Route(login);
                window.WindowState = cruiseWindow.WindowState;
                window.Top = cruiseWindow.Top;
                window.Left = cruiseWindow.Left;
                window.Height = cruiseWindow.Height;
                window.Width = cruiseWindow.Width;
                window.Show();
                cruiseWindow.Close();
            });
        }

        private void setAddCommand()
        {
            Add = new RelayCommand(obj =>
            {
                for (int i = 0; i < 7; i++)
                {
                    if (Days[i])
                    {
                        CruiseModel cruise = new CruiseModel();
                        cruise.Time = currentTime;
                        cruise.day = i + 1;
                        cruise.EndingDate = currentEnding;
                        if (currentStart == "")
                        {
                            cruise.first = DateTime.Now.Date;
                            cruise.StartDate = "";
                        }
                        else
                        {
                            DateTime buffer = DateTime.Parse(currentStart);
                            if (buffer < DateTime.Now.Date)
                            {
                                cruise.first = DateTime.Now.Date;
                                cruise.StartDate = "";
                            }
                            else
                            {
                                cruise.StartDate = currentStart;
                                if (buffer.TimeOfDay.TotalMinutes > DateTime.Parse(cruise.Time).TimeOfDay.TotalMinutes)
                                {
                                    buffer.AddDays(1);
                                    cruise.StartDate = buffer.ToString().Substring(0, 10);
                                }
                                cruise.first = buffer;
                            }
                        }
                        if (cruise.first.TimeOfDay.TotalMinutes > DateTime.Parse(cruise.Time).TimeOfDay.TotalMinutes)
                        {
                            cruise.first.AddDays(1);
                        }
                        if (currentEnding != "")
                        {
                            DateTime buffer = DateTime.Parse(cruise.EndingDate);
                            if (buffer.Date <= cruise.first)
                            {
                                cruise.EndingDate = "";
                            }
                        }
                        cruise.driver_id = currentDriver.driver_id;
                        cruise.transport_id = currentTransport.transport_id;
                        cruise.route_id = routeId;
                        bool f = true;
                        foreach (CruisesModel cr in AllCruises)
                        {
                            if (cr.Comparer(cruise))
                            {
                                if (cr.daysMask[i])
                                {
                                    f = false;
                                    break;
                                }    
                            }
                        }
                        if (f)
                        {
                            db.addCruise(cruise);
                        }
                    }
                }
                setAllCruises();
            },
            obj => { return DateTime.TryParse(currentTime, out DateTime number) && CurrentTime.Length == 5 && ((DateTime.TryParse(currentEnding, out DateTime secondNumber) && CurrentEnding.Length == 10) || currentEnding == "") && ((DateTime.TryParse(currentStart, out DateTime thirdNumber) && currentStart.Length == 10) || currentStart == "") && CurrentDriver != null && CurrentTransport != null; });
        }

        private void setAllCruises()
        {
            List<CruiseModel> cruiseList = db.getCruisesForRoute(routeId, showHidden);
            AllCruises = new ObservableCollection<CruisesModel>();
            List<CruisesModel> buffer = new List<CruisesModel>();
            foreach (CruiseModel cruise in cruiseList)
            {
                bool flag = false;
                foreach (CruisesModel cr in buffer)
                {
                    if (cr.Comparer(cruise))
                    {
                        cr.Add(cruise);
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                {
                    buffer.Add(new CruisesModel(cruise));
                }
            }
            AllCruises = new ObservableCollection<CruisesModel>(buffer.OrderBy(x => x.cruises.First().Time));
            CurrentTime = "";
            CurrentEnding = "";
            CurrentStart = "";
            AllTransport = new ObservableCollection<TransportModel>(db.getTransports(false));
            AllDrivers = new ObservableCollection<DriverModel>(db.getDrivers(false));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
