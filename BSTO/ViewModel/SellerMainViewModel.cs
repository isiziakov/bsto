﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BSTO.ViewModel
{
    public class SellerMainViewModel
    {
        private SellerMain window;
        private string login;

        public SellerMainViewModel(SellerMain main, string userLogin)
        {
            login = userLogin;
            window = main;
            setCommands();
        }

        private void setCommands()
        {
            setCloseCommand();
            setOpenSheduleCommand();
            setOpenSearchCommand();
            setOpenReturnCommand();
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                window.Close();
            });
        }

        private void setOpenSheduleCommand()
        {
            OpenShedule = new RelayCommand(obj =>
            {
                OpenWindow(new SellerSchedule(login));
            });
        }
        private void setOpenSearchCommand()
        {
            OpenSearch = new RelayCommand(obj =>
            {
                OpenWindow(new Search(login));
            });
        }
        private void setOpenReturnCommand()
        {
            OpenReturn = new RelayCommand(obj =>
            {
                OpenWindow(new Return(login));
            });
        }

        public void OpenWindow(Window newWindow)
        {
            newWindow.WindowState = window.WindowState;
            newWindow.Top = window.Top;
            newWindow.Left = window.Left;
            newWindow.Height = window.Height;
            newWindow.Width = window.Width;
            newWindow.Show();
            window.Close();
        }

        public RelayCommand Close { get; set; }
        public RelayCommand OpenShedule { get; set; }
        public RelayCommand OpenSearch { get; set; }
        public RelayCommand OpenReturn { get; set; }
    }
}
