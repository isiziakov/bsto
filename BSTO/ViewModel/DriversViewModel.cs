﻿using BSTO.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BSTO.ViewModel
{
    public class DriversViewModel : INotifyPropertyChanged
    {
        private DBOperations db;
        private string login;
        private Drivers driversWindow;

        private bool showHidden = false;
        public bool ShowHidden
        {
            get { return showHidden; }
            set
            {
                showHidden = value;
                setAllDrivers();
                OnPropertyChanged("ShowHidden");
            }
        }
        private ObservableCollection<DriverModel> allDrivers;
        public ObservableCollection<DriverModel> AllDrivers
        {
            get { return allDrivers; }
            set
            {
                allDrivers = value;
                OnPropertyChanged("AllDrivers");
            }
        }
        private DriverModel selectedDriver;
        public DriverModel SelectedDriver
        {
            get { return selectedDriver; }
            set
            {
                selectedDriver = value;
                if (selectedDriver != null)
                {
                    Name = selectedDriver.Name;
                    Family = selectedDriver.Family;
                    Surname = selectedDriver.Surname;
                }
                OnPropertyChanged("SelectedDriver");
            }
        }

        private string searchingContext;
        public string SearchingContext
        {
            get { return searchingContext; }
            set
            {
                searchingContext = value;
                setAllDrivers();
                OnPropertyChanged("SearchingContext");
            }
        }

        private string family { get; set; }
        public string Family
        {
            get { return family; }
            set
            {
                family = value;
                OnPropertyChanged("Family");
            }
        }

        private string name { get; set; }
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        private string surname { get; set; }
        public string Surname
        {
            get { return surname; }
            set
            {
                surname = value;
                OnPropertyChanged("Surname");
            }
        }

        public RelayCommand Close { get; set; }

        public RelayCommand Add { get; set; }

        public RelayCommand Update { get; set; }

        public RelayCommand HideShow { get; set; }

        public DriversViewModel(String login, Drivers driversWindow)
        {
            this.login = login;
            this.driversWindow = driversWindow;
            searchingContext = "";
            db = new DBOperations();
            setCommands();
            setAllDrivers();
        }

        private void setCommands()
        {
            setCloseCommand();
            setAddCommand();
            setUpdateCommand();
            setHideShowCommand();
        }

        private void setHideShowCommand()
        {
            HideShow = new RelayCommand(obj =>
            {
                SelectedDriver.Hidden = (SelectedDriver.Hidden + 1) % 2;
                db.updateDriver(selectedDriver);
                if (showHidden == false && SelectedDriver.Hidden == 1)
                {
                    AllDrivers.Remove(SelectedDriver);
                }
            },
            obj => { return selectedDriver != null; });
        }
        private void setUpdateCommand()
        {
            Update = new RelayCommand(obj =>
            {
                SelectedDriver.FIO = Family.Replace(" ", "") + ' ' + Name.Replace(" ", "");
                if (Surname != "")
                {
                    SelectedDriver.FIO += (' ' + Surname.Replace(" ", ""));
                }
                db.updateDriver(selectedDriver);
            },
            obj => { return name != "" && family != "" && selectedDriver != null; });
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                Window window = new AdminMain(login);
                window.WindowState = driversWindow.WindowState;
                window.Top = driversWindow.Top;
                window.Left = driversWindow.Left;
                window.Height = driversWindow.Height;
                window.Width = driversWindow.Width;
                window.Show();
                driversWindow.Close();
            });
        }

        private void setAddCommand()
        {
            Add = new RelayCommand(obj =>
            {
                DriverModel driver = new DriverModel();
                driver.FIO = Family.Replace(" ", "") + ' ' + Name.Replace(" ", "");
                if (Surname != "")
                {
                    driver.FIO += (' ' + Surname.Replace(" ", ""));
                }
                driver.Hidden = 0;
                driver.driver_id = db.addDriver(driver);
                AllDrivers.Insert(0, driver);
                SelectedDriver = driver;
            },
            obj => { return name != "" && family != ""; });
        }

        private void setAllDrivers()
        {
            List<DriverModel> driversList = db.getDriversWithContext(showHidden, searchingContext);
            if (driversList == null)
            {
                if (AllDrivers == null)
                {
                    AllDrivers = new ObservableCollection<DriverModel>();
                }
            }
            else
            {
                AllDrivers = new ObservableCollection<DriverModel>(driversList);
            }
            Name = "";
            Surname = "";
            Family = "";
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
