﻿using BSTO.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BSTO.ViewModel
{
    public class ReturnViewModel : INotifyPropertyChanged
    {
        private DBOperations db;
        private string login;
        private Window window;
        private TicketModel ticket;

        private string number;
        public string Number
        {
            get { return number; }
            set
            {
                number = value;
                if (int.TryParse(number, out int r))
                {
                    ticket = db.getTicketById(int.Parse(number));
                    if (ticket != null)
                    {
                        if (ticket.closed || ticket.rplace || ticket.rtime || ticket.returned)
                        {
                            Cost = ticket.Cost.ToString();
                            if (ticket.rplace)
                            {
                                Status = "Изменение места"; 
                            }
                            if (ticket.rtime)
                            {
                                Status = "Изменение времени";
                            }
                            if (ticket.closed)
                            {
                                Status = "Поездка отменена";
                            }
                            if (ticket.returned)
                            {
                                Status = "Выполнен возврат";
                                Cost = "-";
                            }
                        }
                        else
                        {
                            TimeSpan time = ticket.start_date - DateTime.Now;
                            if (time.TotalMinutes >= 120)
                            {
                                Status = "Более 2 часов до отправки рейса. Возврат 95%.";
                                Cost = ((int)(ticket.Cost * 0.95)).ToString();
                            }
                            else
                            {
                                if (time.TotalMinutes >= 0)
                                {
                                    Status = "Менее 2 часов до отправки рейса. Возврат 85%.";
                                    Cost = ((int)(ticket.Cost * 0.85)).ToString();
                                }
                                else
                                {
                                    Status = "";
                                    Cost = "";
                                }
                            }
                        }
                    }
                    else
                    {
                        Cost = "";
                        Status = "";
                    }
                }
                else
                {
                    FIO = "";
                    Cost = "";
                    Status = "";
                }
                OnPropertyChanged("Number");
            }
        }

        private string fio;
        public string FIO
        {
            get { return fio; }
            set
            {
                fio = value;
                OnPropertyChanged("FIO");
            }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set
            {
                status = value;
                OnPropertyChanged("Status");
            }
        }

        private string cost;
        public string Cost
        {
            get { return cost; }
            set
            {
                cost = value;
                OnPropertyChanged("Cost");
            }
        }

        public RelayCommand Close { get; set; }
        public RelayCommand Return { get; set; }

        public ReturnViewModel(string login, Window window)
        {
            Cost = "";
            Status = "";
            FIO = "";
            Number = "";
            this.login = login;
            this.window = window;
            db = new DBOperations();
            setCommands();
        }

        private void setCommands()
        {
            setCloseCommand();
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                Window newWindow = new SellerMain(login);
                newWindow.WindowState = window.WindowState;
                newWindow.Top = window.Top;
                newWindow.Left = window.Left;
                newWindow.Height = window.Height;
                newWindow.Width = window.Width;
                newWindow.Show();
                window.Close();
            });
            Return = new RelayCommand(obj =>
            {
                if (FIO.ToLower() == ticket.FIO.ToLower())
                {
                    ticket.Cost -= int.Parse(Cost);
                    ticket.returned = true;
                    db.updateTicket(ticket);
                    MessageBox.Show("Выполнен возврат");
                    Number = "";
                }
                else
                {
                    MessageBox.Show("Неверное ФИО покупателя");
                }
            }, func => { return int.TryParse(Cost, out int b); });
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
