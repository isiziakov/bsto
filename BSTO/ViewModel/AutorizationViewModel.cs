﻿using BSTO.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace BSTO.ViewModel
{
    public class AutorizationViewModel : INotifyPropertyChanged
    {
        private string login;
        public string Login
        {
            get { return login; }
            set
            {
                login = value;
                OnPropertyChanged("Login");
            }
        }

        private string wrongPassword = "Hidden";
        public string WrongPassword
        {
            get { return wrongPassword; }
            set
            {
                wrongPassword = value;
                OnPropertyChanged("WrongPassword");
            }
        }

        private DBOperations db;
        private MainWindow window;
        public AutorizationViewModel(MainWindow autorization)
        {
            window = autorization;
            db = new DBOperations();
            Login = "admin_test"; // only for tests
            setEnterCommand();
        }

        private void setEnterCommand()
        {
            Enter = new RelayCommand(obj =>
            {
                PasswordBox password = obj as PasswordBox;
                if (password != null)
                {
                    UserModel user = db.getUserByLogin(login);
                    if (user != null && user.password == password.Password && user.hidden == 0)
                    {
                        string type = db.getUserType(user.type_id);
                        if (type != null)
                        {
                            if (type == "Администратор")
                            {
                                AdminMain adminWindow = new AdminMain(user.login);
                                adminWindow.Show();
                                window.Close();
                            }
                            else
                            {
                                SellerMain sellerWindow = new SellerMain(user.login);
                                sellerWindow.Show();
                                window.Close();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Тип пользователя не определен");
                        }
                    }
                    else
                    {
                        password.Password = "";
                        WrongPassword = "Visible";
                    }
                }
                else WrongPassword = "Visible";
            },
                  obj => { return login != ""; });
        }

        public RelayCommand Enter { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
