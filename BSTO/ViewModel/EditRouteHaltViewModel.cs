﻿using BSTO.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BSTO.ViewModel
{
    public class EditRouteHaltViewModel : INotifyPropertyChanged
    {
        private int routeId;
        private DBOperations db;
        private string login;
        private Window currentWindow;

        public ObservableCollection<RouteHaltModel> AllRouteHalts { get; set; }

        private RouteHaltModel selectedRouteHalt;
        public RouteHaltModel SelectedRouteHalt
        {
            get { return selectedRouteHalt; }
            set
            {
                selectedRouteHalt = value;
                if (selectedRouteHalt != null)
                {
                    CurrentCost = selectedRouteHalt.Cost.ToString();
                    CurrentTime = selectedRouteHalt.Time.ToString();
                }
                OnPropertyChanged("SelectedRouteHalt");
            }
        }

        private string currentTime;
        public string CurrentTime
        {
            get { return currentTime; }
            set
            {
                currentTime = value;
                OnPropertyChanged("CurrentTime");
            }
        }

        private string currentCost;
        public string CurrentCost
        {
            get { return currentCost; }
            set
            {
                currentCost = value;
                OnPropertyChanged("CurrentCost");
            }
        }

        public RelayCommand Close { get; set; }

        public RelayCommand Update { get; set; }

        public RelayCommand HideShow { get; set; }

        public EditRouteHaltViewModel(string login, Window window, int routeId)
        {
            this.routeId = routeId;
            this.login = login;
            currentWindow = window;
            db = new DBOperations();
            setCommands();
            setAllRouteHalts();
        }

        private void setCommands()
        {
            setCloseCommand();
            setUpdateCommand();
            setHideShowCommand();
        }

        private void setHideShowCommand()
        {
            HideShow = new RelayCommand(obj =>
            {
                SelectedRouteHalt.Hidden = (SelectedRouteHalt.Hidden + 1) % 2;
                db.updateRouteHalt(selectedRouteHalt);
                List<TicketModel> tickets = db.getTicketsForRoute(routeId).Where(i => i.date > DateTime.Now).ToList();
                foreach (TicketModel t in tickets)
                {
                    if (t.start_halt_id == SelectedRouteHalt.RouteHaltId || t.end_halt_id == SelectedRouteHalt.RouteHaltId)
                    {
                        t.closed = true;
                        db.updateTicket(t);
                    }
                }
            },
            obj => { return selectedRouteHalt != null; });
        }
        private void setUpdateCommand()
        {
            Update = new RelayCommand(obj =>
            {
                int time = int.Parse(CurrentTime) - SelectedRouteHalt.Time;
                SelectedRouteHalt.Time = int.Parse(CurrentTime);
                SelectedRouteHalt.Cost = int.Parse(CurrentCost);
                if (time != 0)
                {
                    List<TicketModel> tickets = db.getTicketsForRoute(routeId).Where(i => i.date > DateTime.Now).ToList();
                    foreach (TicketModel t in tickets)
                    {
                        if (t.start_halt_id == SelectedRouteHalt.RouteHaltId)
                        {
                            t.start_date = t.start_date.AddMinutes(time);
                            t.rtime = true;
                            db.updateTicket(t);
                        }
                        if (t.end_halt_id == SelectedRouteHalt.RouteHaltId)
                        {
                            t.rtime = true;
                            db.updateTicket(t);
                        }
                    }
                }
                db.updateRouteHalt(SelectedRouteHalt);
            },
            obj => { return int.TryParse(currentCost, out int number) && int.Parse(currentCost) > 0 && int.TryParse(currentTime, out int number2) && int.Parse(currentTime) > 0 && selectedRouteHalt != null && AllRouteHalts.IndexOf(selectedRouteHalt) > 0; });
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                Window window = new Route(login);
                window.WindowState = currentWindow.WindowState;
                window.Top = currentWindow.Top;
                window.Left = currentWindow.Left;
                window.Height = currentWindow.Height;
                window.Width = currentWindow.Width;
                window.Show();
                currentWindow.Close();
            });
        }

        private void setAllRouteHalts()
        {
            List<RouteHaltModel> haltsList = db.getRouteHaltsByRouteId(true, routeId);
            AllRouteHalts = new ObservableCollection<RouteHaltModel>(haltsList);
            foreach (RouteHaltModel r in AllRouteHalts)
            {
                HaltModel buffer = db.getHaltById(r.HaltId);
                r.Locality = db.getLocality(buffer.locality_id).locality_name;
                r.Adress = buffer.Adress;
            }
            CurrentTime = "";
            CurrentCost = "";
            SelectedRouteHalt = null;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
