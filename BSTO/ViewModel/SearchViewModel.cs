﻿using BSTO.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BSTO.ViewModel
{
    public class SearchViewModel : INotifyPropertyChanged
    {
        private DBOperations db;
        private string login;
        private Window window;

        public List<LocalityModel> Localities { get; set; }

        private List<HaltModel> allHalts;

        private ObservableCollection<HaltModel> startHalts;
        public ObservableCollection<HaltModel> StartHalts
        {
            get { return startHalts; }
            set
            {
                startHalts = value;
                OnPropertyChanged("StartHalts");
            }
        }

        private ObservableCollection<HaltModel> endHalts;
        public ObservableCollection<HaltModel> EndHalts
        {
            get { return endHalts; }
            set
            {
                endHalts = value;
                OnPropertyChanged("EndHalts");
            }
        }

        private ObservableCollection<TravellModel> allTravell;
        public ObservableCollection<TravellModel> AllTravell
        {
            get { return allTravell; }
            set
            {
                allTravell = value;
                OnPropertyChanged("AllTravell");
            }
        }

        private HaltModel selectedStartHalt;
        public HaltModel SelectedStartHalt
        {
            get { return selectedStartHalt; }
            set
            {
                selectedStartHalt = value;
                setCruises();
                OnPropertyChanged("SelectedStartHalt");
            }
        }

        private HaltModel selectedEndHalt;
        public HaltModel SelectedEndHalt
        {
            get { return selectedEndHalt; }
            set
            {
                selectedEndHalt = value;
                setCruises();
                OnPropertyChanged("SelectedEndHalt");
            }
        }

        private TravellModel selectedTravell;
        public TravellModel SelectedTravell
        {
            get { return selectedTravell; }
            set
            {
                selectedTravell = value;
                OnPropertyChanged("SelectedTravell");
            }
        }
       
        public DateTime DisplayDateStart { get; set; } = DateTime.Now;

        public DateTime DisplayDateEnd { get; set; } = DateTime.Now.AddDays(14);

        private DateTime selectedDate = DateTime.Now;
        public DateTime SelectedDate
        {
            get { return selectedDate; }
            set
            {
                selectedDate = value;
                setCruises();
                OnPropertyChanged("SelectedDate");
            }
        }

        private string startTime = "";
        public string StartTime
        {
            get { return startTime; }
            set
            {
                startTime = value;
                setCruises();
                OnPropertyChanged("StartTime");
            }
        }

        private string endTime = "";
        public string EndTime
        {
            get { return endTime; }
            set
            {
                endTime = value;
                setCruises();
                OnPropertyChanged("EndTime");
            }
        }

        private LocalityModel currentStart;
        public LocalityModel CurrentStart
        {
            get { return currentStart; }
            set
            {
                currentStart = value;
                if (currentStart != null)
                {
                    SelectedStartHalt = null;
                    StartHalts = new ObservableCollection<HaltModel>(allHalts.Where(i => i.locality_id == currentStart.locality_id).ToList());
                }
                OnPropertyChanged("CurrentStart");
            }
        }

        private LocalityModel currentEnding;
        public LocalityModel CurrentEnding
        {
            get { return currentEnding; }
            set
            {
                currentEnding = value;
                if (currentEnding != null)
                {
                    SelectedEndHalt = null;
                    EndHalts = new ObservableCollection<HaltModel>(allHalts.Where(i => i.locality_id == currentEnding.locality_id).ToList());
                }
                OnPropertyChanged("CurrentEnding");
            }
        }

        public RelayCommand Close { get; set; }

        public RelayCommand Sell { get; set; }


        public SearchViewModel(string login, Window window)
        {
            this.login = login;
            this.window = window;
            db = new DBOperations();
            Localities = db.getLocalities();
            setCommands();
            setAllHalts();
        }

        private void setAllHalts()
        {
            allHalts = db.getHalts(false);
        }

        private void setCruises()
        {
            if (SelectedStartHalt != null && SelectedEndHalt != null)
            {
                List<CruiseModel> buffer = db.getCruisesForSearch(SelectedStartHalt.halt_id, SelectedEndHalt.halt_id);
                buffer = buffer.Where(i => (i.StartDate == "" || DateTime.Parse(i.StartDate) <= SelectedDate) && (i.EndingDate == "" || DateTime.Parse(i.EndingDate) >= SelectedDate)).ToList();
                int day = DaysConverter.EStringToInt(SelectedDate.DayOfWeek.ToString());
                List<TravellModel> travells = new List<TravellModel>();
                foreach (CruiseModel cr in buffer)
                {
                    var rhalts = db.getRouteHaltsByRouteId(false, cr.route_id);
                    DateTime date = SelectedDate.Date;
                    if (cr.day == day)
                    {
                        date += TimeSpan.Parse(cr.Time);
                        date = date.AddMinutes(rhalts.Where(i => i.HaltId == SelectedStartHalt.halt_id).FirstOrDefault().Time);
                    }
                    if (cr.day > day)
                    {
                        date = date.AddDays(-7 + (cr.day - day));
                        date += TimeSpan.Parse(cr.Time);
                        date = date.AddMinutes(rhalts.Where(i => i.HaltId == SelectedStartHalt.halt_id).FirstOrDefault().Time);
                    }
                    if (cr.day < day)
                    {
                        date = date.AddDays(cr.day - day);
                        date += TimeSpan.Parse(cr.Time);
                        date = date.AddMinutes(rhalts.Where(i => i.HaltId == SelectedStartHalt.halt_id).FirstOrDefault().Time);
                    }
                    if (date.Date == SelectedDate.Date)
                    {
                        int start = rhalts.Where(i => i.HaltId == SelectedStartHalt.halt_id).FirstOrDefault().RouteHaltId;
                        int end = rhalts.Where(i => i.HaltId == SelectedEndHalt.halt_id).FirstOrDefault().RouteHaltId;
                        travells.Add(new TravellModel(cr, start, end, date));
                    }
                }
                if (startTime != "" && endTime != "" && endTime.Length == 5 && startTime.Length == 5 && DateTime.TryParse(startTime, out DateTime test) && DateTime.TryParse(endTime, out DateTime test2))
                {
                    if (startTime.CompareTo(endTime) > 0)
                    {
                        travells = travells.Where(i => i.Time.CompareTo(startTime) >= 0 || i.Time.CompareTo(endTime) <= 0).ToList();
                    }
                    else
                    {
                        travells = travells.Where(i => i.Time.CompareTo(startTime) >= 0 && i.Time.CompareTo(endTime) <= 0).ToList();
                    }
                }
                AllTravell = new ObservableCollection<TravellModel>(travells);
            }
        }

        private void setCommands()
        {
            setCloseCommand();
            setSellCommand();
        }

        private void setSellCommand()
        {
            Sell = new RelayCommand(obj =>
            {
                var rhalts = db.getRouteHaltsByRouteId(false, selectedTravell.RouteId);
                int time = rhalts.Where(i => i.HaltId == SelectedStartHalt.halt_id).FirstOrDefault().Time;
                int start = rhalts.Where(i => i.HaltId == SelectedStartHalt.halt_id).FirstOrDefault().RouteHaltId;
                int end = rhalts.Where(i => i.HaltId == SelectedEndHalt.halt_id).FirstOrDefault().RouteHaltId;
                OpenWindow(new SellingPage(login, start, end, selectedTravell.Date.AddMinutes(-time), selectedTravell.Date, selectedTravell.cruise_id, selectedTravell.RouteId));
            },
            obj => { 
                return selectedTravell != null && selectedTravell.Date > DateTime.Now.AddMinutes(8); 
            });
        }

        public void OpenWindow(Window newWindow)
        {
            newWindow.WindowState = window.WindowState;
            newWindow.Top = window.Top;
            newWindow.Left = window.Left;
            newWindow.Height = window.Height;
            newWindow.Width = window.Width;
            newWindow.Show();
            window.Close();
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                Window window = new SellerMain(login);
                window.WindowState = this.window.WindowState;
                window.Top = this.window.Top;
                window.Left = this.window.Left;
                window.Height = this.window.Height;
                window.Width = this.window.Width;
                window.Show();
                this.window.Close();
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
