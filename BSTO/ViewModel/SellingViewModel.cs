﻿using BSTO.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace BSTO.ViewModel
{
    public class SellingViewModel : INotifyPropertyChanged
    {
        private DBOperations db;
        private string login;
        private Window window;

        private int startHaltId;
        private int endHaltId;
        private int cruiseId;
        private DateTime dateTime;
        private DateTime startDate;

        private int withoutPers;

        private List<BusyPlaceModel> busyPlaces = new List<BusyPlaceModel>();

        private ObservableCollection<int> places;
        public ObservableCollection<int> Places
        {
            get { return places; }
            set
            {
                places = value;
                OnPropertyChanged("Places");
            }
        }

        private int selectedIndex;
        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                selectedIndex = value;
                OnPropertyChanged("SelectedIndex");
            }
        }

        public ObservableCollection<TicketModel> Tickets { get; set; }

        private TicketModel selectedTicket;
        public TicketModel SelectedTicket
        {
            get { return selectedTicket; }
            set
            {
                selectedTicket = value;
                if (selectedTicket != null)
                {
                    CurrentPassport = SelectedTicket.Passport;
                    CurrentFIO = SelectedTicket.FIO;
                }
                OnPropertyChanged("SelectedTicked");
            }
        }

        private string currentFIO;
        public string CurrentFIO
        {
            get { return currentFIO; }
            set
            {
                currentFIO = value;
                OnPropertyChanged("CurrentFIO");
            }
        }

        private string currentPassport;
        public string CurrentPassport
        {
            get { return currentPassport; }
            set
            {
                currentPassport = value;
                OnPropertyChanged("CurrentPassport");
            }
        }

        public string CurrentTime { get; set; }

        public string CurrentDate { get; set; }

        public string CurrentRoute { get; set; }

        public RelayCommand Close { get; set; }

        public RelayCommand Sell { get; set; }
        public RelayCommand Add { get; set; }
        public RelayCommand Delete { get; set; }
        public RelayCommand Update { get; set; }

        public SellingViewModel(string login, Window window, int startHaltId, int endHaltId, DateTime dateTime, DateTime startDate, int cruiseId, int routeId)
        {
            withoutPers = 0;
            CurrentFIO = "";
            CurrentPassport = "";
            this.login = login;
            this.window = window;
            this.startHaltId = startHaltId;
            this.endHaltId = endHaltId;
            this.dateTime = dateTime;
            this.cruiseId = cruiseId;
            this.startDate = startDate;
            CurrentRoute = routeId.ToString();
            CurrentTime = startDate.TimeOfDay.ToString().Substring(0, 5);
            CurrentDate = startDate.Date.ToString().Substring(0, 10);
            db = new DBOperations();
            Tickets = new ObservableCollection<TicketModel>();
            setCommands();
            setAllPlaces();
        }

        private void setCommands()
        {
            setCloseCommand();
            setAddCommand();
            setSellCommand();
            setUpdateCommand();
            setDeleteCommand();
        }

        private void setSellCommand()
        {
            Sell = new RelayCommand(obj =>
            {
                bool success = true;
                int r = 1;
                foreach (TicketModel t in Tickets)
                {
                    r = t.Sell(login);
                    if (r == -1)
                    {
                        System.Windows.Forms.MessageBox.Show("Ошибка авторизации");
                        success = false;
                        break;
                    }
                    if (r == 0)
                    {
                        System.Windows.Forms.MessageBox.Show("Ошибка добавления билета");
                        success = false;
                        break;
                    }
                    t.ticket_id = r;
                    t.Print(login);
                }
                if (success)
                {
                    close();
                }
            },
            obj => { 
                return Tickets.Count != 0 && withoutPers == 0 && DateTime.Now < startDate; 
            });
        }
        private void setDeleteCommand()
        {
            Delete = new RelayCommand(obj =>
            {
                db.deleteBusyPlace(busyPlaces.Where(i => i.cruiseId == cruiseId && i.place == selectedTicket.Place).First());
                if (SelectedTicket.FIO != "")
                {
                    withoutPers--;
                }
                Tickets.Remove(SelectedTicket);
                setAllPlaces();
            },
            obj => { return selectedTicket != null; });
        }
        private void setUpdateCommand()
        {
            Update = new RelayCommand(obj =>
            {
                if (SelectedTicket.FIO == "")
                {
                    withoutPers--;
                }
                SelectedTicket.SetPerson(currentFIO, currentPassport); 
            },
            obj => { return currentFIO != "" && currentPassport != "" && selectedTicket != null; });
        }

        private void close()
        {
            foreach (BusyPlaceModel b in busyPlaces)
            {
                db.deleteBusyPlace(b);
            }
            Window window = new SellerMain(login);
            window.WindowState = this.window.WindowState;
            window.Top = this.window.Top;
            window.Left = this.window.Left;
            window.Height = this.window.Height;
            window.Width = this.window.Width;
            window.Show();
            this.window.Close();
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                close();
            });
        }

        private void setAddCommand()
        {
            Add = new RelayCommand(obj =>
            {
                Tickets.Add(new TicketModel(db.getCruiseById(cruiseId), startHaltId, endHaltId, dateTime, startDate, Places[selectedIndex], db));
                busyPlaces.Add(new BusyPlaceModel(cruiseId, Places[selectedIndex], dateTime.Date, startHaltId, endHaltId));
                db.addBusyPlace(busyPlaces.Last());
                withoutPers++;
                setAllPlaces();
            },
            obj => { return selectedIndex != -1 ; });
        }

        private void setAllPlaces()
        {
            Places = new ObservableCollection<int>(db.getFreePlaces(cruiseId, dateTime.Date, startHaltId, endHaltId));
            SelectedIndex = -1;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
