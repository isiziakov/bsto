﻿using BSTO.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BSTO.ViewModel
{
    public class SellerScheduleViewModel : INotifyPropertyChanged
    {
        private DBOperations db;
        private string login;
        private SellerSchedule window;

        private List<RouteHaltModel> allHalts;

        private ObservableCollection<RouteHaltModel> startHalts;
        public ObservableCollection<RouteHaltModel> StartHalts
        {
            get { return startHalts; }
            set
            {
                startHalts = value;
                OnPropertyChanged("StartHalts");
            }
        }

        private ObservableCollection<RouteHaltModel> endHalts;
        public ObservableCollection<RouteHaltModel> EndHalts
        {
            get { return endHalts; }
            set
            {
                endHalts = value;
                OnPropertyChanged("EndHalts");
            }
        }

        private ObservableCollection<RouteModel> allRoutes;
        public ObservableCollection<RouteModel> AllRoutes
        {
            get { return allRoutes; }
            set
            {
                allRoutes = value;
                OnPropertyChanged("AllRoutes");
            }
        }

        private ObservableCollection<LocalityModel> localities;
        public ObservableCollection<LocalityModel> Localities
        {
            get { return localities; }
            set
            {
                localities = value;
                OnPropertyChanged("Localities");
            }
        }

        private RouteHaltModel selectedStartHalt;
        public RouteHaltModel SelectedStartHalt
        {
            get { return selectedStartHalt; }
            set
            {
                selectedStartHalt = value;
                if (SelectedStartHalt != null )
                {
                    setCurrentTime();
                    if (currentEnding != null)
                    {
                        if (SelectedEndHalt != null)
                        {
                            if (SelectedStartHalt.Number >= SelectedEndHalt.Number)
                            {
                                SelectedEndHalt = null;
                            }
                            else
                            {
                                setCost();
                            }
                        }
                        EndHalts = new ObservableCollection<RouteHaltModel>(allHalts.Where(i => i.Locality == currentEnding.locality_name && i.Number > SelectedStartHalt.Number).ToList());
                    }
                }
                OnPropertyChanged("SelectedStartHalt");
            }
        }

        private RouteHaltModel selectedEndHalt;
        public RouteHaltModel SelectedEndHalt
        {
            get { return selectedEndHalt; }
            set
            {
                selectedEndHalt = value;
                if (SelectedEndHalt != null)
                {
                    setCost();
                    setPlaces();
                }
                else
                {
                    CurrentCost = "";
                    CurrentTime = "";
                    CurrentPlaces = "";
                }
                OnPropertyChanged("SelectedEndHalt");
            }
        }

        private void setCurrentTime()
        {
            CurrentTime = DateTime.Parse(selectedCruise.Time).AddMinutes(SelectedStartHalt.Time).TimeOfDay.ToString().Substring(0, 5);
        }

        private void setCost()
        {
            int startCost = allHalts.Find(i => i.Number == selectedStartHalt.Number).Cost;
            int endCost = allHalts.Find(i => i.Number == selectedEndHalt.Number).Cost;
            CurrentCost = (endCost - startCost).ToString();
        }

        private void setPlaces()
        {
            int places = selectedCruise.Transport.Places_count;
            places -= db.getNotFreePlacesCount(selectedCruise.cruise_id, selectedDate, selectedStartHalt.RouteHaltId, selectedEndHalt.RouteHaltId);
            CurrentPlaces = places.ToString();
        }

        private CruiseModel selectedCruise;
        public CruiseModel SelectedCruise
        {
            get { return selectedCruise; }
            set
            {
                selectedCruise = null;
                OnPropertyChanged("SelectedCruise");
                int id = -1;
                if (selectedCruise != null)
                {
                    id = selectedCruise.route_id;
                }
                selectedCruise = value;
                if (SelectedCruise == null || id != SelectedCruise.route_id)
                {
                    CurrentCost = "";
                    CurrentPlaces = "";
                    CurrentStart = null;
                    CurrentEnding = null;
                    StartHalts = null;
                    EndHalts = null;
                    SelectedEndHalt = null;
                    SelectedStartHalt = null;
                    if (SelectedCruise != null)
                    {
                        setAllHalts();
                        setLocalities();
                    }

                }
                if (SelectedCruise != null && SelectedStartHalt != null)
                {
                    setCurrentTime();
                }
                OnPropertyChanged("SelectedCruise");
            }
        }

        private string currentTime;
        public string CurrentTime
        {
            get { return currentTime; }
            set
            {
                currentTime = value;
                OnPropertyChanged("CurrentTime");
            }
        }

        private string currentCost;
        public string CurrentCost
        {
            get { return currentCost; }
            set
            {
                currentCost = value;
                OnPropertyChanged("CurrentCost");
            }
        }

        private string currentPlaces;
        public string CurrentPlaces
        {
            get { return currentPlaces; }
            set
            {
                currentPlaces = value;
                OnPropertyChanged("CurrentPlaces");
            }
        }

        public DateTime DisplayDateStart { get; set; } = DateTime.Now.AddDays(-3);

        public DateTime DisplayDateEnd { get; set; } = DateTime.Now.AddDays(14);

        private DateTime selectedDate = DateTime.Now;
        public DateTime SelectedDate
        {
            get { return selectedDate; }
            set
            {
                selectedDate = value;
                setAllRoutes();
                OnPropertyChanged("SelectedDate");
            }
        }

        private LocalityModel currentStart;
        public LocalityModel CurrentStart
        {
            get { return currentStart; }
            set
            {
                currentStart = value;
                if (currentStart != null && allHalts != null)
                {
                    StartHalts = new ObservableCollection<RouteHaltModel>(allHalts.Where(i => i.Locality == currentStart.locality_name && i.Number != allHalts[allHalts.Count - 1].Number).ToList());
                }
                OnPropertyChanged("CurrentStart");
            }
        }

        private LocalityModel currentEnding;
        public LocalityModel CurrentEnding
        {
            get { return currentEnding; }
            set
            {
                currentEnding = value;
                if (SelectedStartHalt != null && currentEnding != null)
                {
                    EndHalts = new ObservableCollection<RouteHaltModel>(allHalts.Where(i => i.Locality == currentEnding.locality_name && i.Number > SelectedStartHalt.Number).ToList());
                }
                OnPropertyChanged("CurrentEnding");
            }
        }

        public RelayCommand Close { get; set; }

        public RelayCommand Sell { get; set; }


        public SellerScheduleViewModel(string login, SellerSchedule window)
        {
            this.login = login;
            this.window = window;
            AllRoutes = new ObservableCollection<RouteModel>();
            db = new DBOperations();
            setCommands();
            setAllRoutes();
        }

        private void setCommands()
        {
            setCloseCommand();
            setSellCommand();
        }
        private void setLocalities()
        {
            List<LocalityModel> buffer = db.getLocalities();
            buffer = buffer.Where(i => allHalts.Find(j => j.Locality == i.locality_name) != null).ToList();
            Localities = new ObservableCollection<LocalityModel>(buffer);
        }
        private void setAllHalts()
        {
            allHalts = db.getAllRouteHaltsByRouteId(SelectedCruise.route_id);
            foreach (RouteHaltModel r in allHalts)
            {
                if (r.Hidden == 0)
                {
                    HaltModel buffer = db.getHaltById(r.HaltId);
                    r.Locality = db.getLocality(buffer.locality_id).locality_name;
                    r.Adress = buffer.Adress;
                }
            }
        }

        private void setSellCommand()
        {
            Sell = new RelayCommand(obj =>
            {
                DateTime buffer = SelectedDate.Date + TimeSpan.Parse(selectedCruise.Time);
                DateTime startDate = buffer.AddMinutes(SelectedStartHalt.Time);
                OpenWindow(new SellingPage(login, selectedStartHalt.RouteHaltId, selectedEndHalt.RouteHaltId, buffer, startDate, selectedCruise.cruise_id, selectedCruise.route_id));
            },
            obj => {
                return selectedCruise != null && selectedEndHalt != null && selectedStartHalt != null && currentPlaces != "0" && ((SelectedDate.Date + TimeSpan.Parse(SelectedCruise.Time)).AddMinutes(selectedStartHalt.Time) > DateTime.Now.AddMinutes(8));
            });
        }

        public void OpenWindow(Window newWindow)
        {
            newWindow.WindowState = window.WindowState;
            newWindow.Top = window.Top;
            newWindow.Left = window.Left;
            newWindow.Height = window.Height;
            newWindow.Width = window.Width;
            newWindow.Show();
            window.Close();
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                Window window = new SellerMain(login);
                window.WindowState = this.window.WindowState;
                window.Top = this.window.Top;
                window.Left = this.window.Left;
                window.Height = this.window.Height;
                window.Width = this.window.Width;
                window.Show();
                this.window.Close();
            });
        }

        private void setAllRoutes()
        {
            List<RouteModel> routeList = db.getRoutesWithContext(false, "");
            AllRoutes.Clear();
            foreach (RouteModel r in routeList)
            {
                List<CruiseModel> cruises = db.getCruisesForRoute(r.Number, false);
                cruises = cruises.Where(i => (!DateTime.TryParse(i.EndingDate, out DateTime buffer) || DateTime.Parse(i.EndingDate) >= SelectedDate) && (!DateTime.TryParse(i.StartDate, out DateTime buffer2) || DateTime.Parse(i.StartDate) <= SelectedDate) && i.day == DaysConverter.EStringToInt(SelectedDate.DayOfWeek.ToString())).ToList();
                if (cruises.Count > 0)
                {
                    r.Cruises = new ObservableCollection<CruiseModel>(cruises);
                }
                else
                {
                    r.Cruises = null;
                }
            }
            AllRoutes = new ObservableCollection<RouteModel>(routeList.Where(i => i.Cruises != null).ToList());
            CurrentEnding = null;
            CurrentCost = "";
            CurrentTime = "";
            CurrentStart = null;
            CurrentPlaces = "";
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
