﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BSTO.ViewModel
{
    public class AdminMainViewModel : INotifyPropertyChanged
    {
        private AdminMain window;
        private string login;

        public AdminMainViewModel(AdminMain main, string userLogin)
        {
            login = userLogin;
            window = main;
            setCommands();
        }

        private void setCommands()
        {
            setCloseCommand();
            setOpenHaltsCommand();
            setOpenDriversCommand();
            setOpenTransportsCommand();
            setOpenRoutesCommand();
            setOpenReportsCommand();
        }

        private void setCloseCommand()
        {
            Close = new RelayCommand(obj =>
            {
                window.Close();
            });
        }

        private void setOpenHaltsCommand()
        {
            OpenHalts = new RelayCommand(obj =>
            {
                OpenWindow(new Halts(login));
            });
        }
        private void setOpenDriversCommand()
        {
            OpenDrivers = new RelayCommand(obj =>
            {
                OpenWindow(new Drivers(login));
            });
        }
        private void setOpenTransportsCommand()
        {
            OpenTransports = new RelayCommand(obj =>
            {
                OpenWindow(new Bus(login));
            });
        }

        private void setOpenRoutesCommand()
        {
            OpenRoutes = new RelayCommand(obj =>
            {
                OpenWindow(new Route(login));
            });
        }

        private void setOpenReportsCommand()
        {
            OpenReports = new RelayCommand(obj =>
            {
                OpenWindow(new Report(login));
            });
        }

        public void OpenWindow(Window newWindow)
        {
            newWindow.WindowState = window.WindowState;
            newWindow.Top = window.Top;
            newWindow.Left = window.Left;
            newWindow.Height = window.Height;
            newWindow.Width = window.Width;
            newWindow.Show();
            window.Close();
        }

        public RelayCommand Close { get; set; }
        public RelayCommand OpenHalts { get; set; }
        public RelayCommand OpenDrivers { get; set; }
        public RelayCommand OpenTransports { get; set; }
        public RelayCommand OpenRoutes { get; set; }
        public RelayCommand OpenReports { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
