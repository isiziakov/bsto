﻿using BSTO.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BSTO
{
    /// <summary>
    /// Логика взаимодействия для SellingPage.xaml
    /// </summary>
    public partial class SellingPage : Window
    {
        public SellingPage(string login, int startHaltId, int endHaltId, DateTime dateTime, DateTime startTime, int cruiseId, int routeId)
        {
            InitializeComponent();
            DataContext = new SellingViewModel(login, this, startHaltId, endHaltId, dateTime, startTime, cruiseId, routeId);
        }
    }
}
