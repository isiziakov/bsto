﻿using BSTO.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BSTO
{
    /// <summary>
    /// Логика взаимодействия для Cruise.xaml
    /// </summary>
    public partial class Cruise : Window
    {
        public Cruise(string login, int routeId)
        {
            InitializeComponent();
            DataContext = new CruiseViewModel(login, this, routeId);
        }
    }
}
