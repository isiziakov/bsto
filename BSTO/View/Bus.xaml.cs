﻿using BSTO.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BSTO
{
    /// <summary>
    /// Логика взаимодействия для Bus.xaml
    /// </summary>
    public partial class Bus : Window
    {
        String login;
        public Bus(String login)
        {
            this.login = login;
            DataContext = new TransportViewModel(login, this);
            InitializeComponent();
        }
    }
}
