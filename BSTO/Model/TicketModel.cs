﻿using System;
using PdfSharp;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using DAL.Entity;

namespace BSTO.Model
{
    public class TicketModel : INotifyPropertyChanged
    {
        public int ticket_id;

        public int seller_id;

        public int route_id;

        public int cruise_id;

        public DateTime date;

        public int transport_id;

        public int driver_id;

        private string fio { get; set; }
        public string FIO
        {
            get { return fio; }
            set
            {
                fio = value;
                OnPropertyChanged("FIO");
            }
        }

        private string passport { get; set; }
        public string Passport
        {
            get { return passport; }
            set
            {
                passport = value;
                OnPropertyChanged("Passport");
            }
        }

        private int place { get; set; }
        public int Place
        {
            get { return place; }
            set
            {
                place = value;
                OnPropertyChanged("Place");
            }
        }

        public int Cost { get; set; }

        public int start_halt_id;

        public bool returned;

        public int end_halt_id;

        public string seller_FIO;

        public string driver_FIO;
        public string transport_number;

        public DateTime selling_time;
        public DateTime start_date;

        public bool closed = false;
        public bool rplace = false;
        public bool rtime = false;

        private DBOperations db;

        public TicketModel(Ticket ticket)
        {
            db = new DBOperations();
            ticket_id = ticket.ticket_id;
            cruise_id = ticket.cruise_id;
            start_halt_id = ticket.start_halt_id;
            end_halt_id = ticket.end_halt_id;
            Cost = ticket.cost;
            returned = ticket.returned;
            rtime = ticket.rtime;
            rplace = ticket.rplace;
            closed = ticket.closed;
            date = ticket.date;
            start_date = ticket.start_date;
            route_id = ticket.route_id;
            driver_id = ticket.driver_id;
            transport_id = ticket.transport_id;
            Place = ticket.place;
            driver_FIO = db.getDriverById(driver_id).FIO;
            transport_number = db.getTransportById(transport_id).number;
            FIO = ticket.FIO;
            Passport = ticket.passport;
            seller_id = ticket.seller_id;
            selling_time = ticket.selling_time;
        }
        public TicketModel(CruiseModel cruise, int start, int end, DateTime date, DateTime startDate, int place, DBOperations db)
        {
            this.db = db;
            cruise_id = cruise.cruise_id;
            start_halt_id = start;
            end_halt_id = end;
            int startCost = db.getRouteHaltCostById(start);
            int endCost = db.getRouteHaltCostById(end);
            Cost = endCost - startCost;
            returned = false;
            this.date = date;
            start_date = startDate;
            route_id = cruise.route_id;
            driver_id = cruise.driver_id;
            transport_id = cruise.transport_id;
            Place = place;
            driver_FIO = cruise.Driver.FIO;
            transport_number = cruise.Transport.Number;
            FIO = "";
            Passport = "";
        }

        public int Sell(string login)
        {
            UserModel seller = db.getUserByLogin(login);
            if (seller != null)
            {
                seller_id = seller.id;
                selling_time = DateTime.Now;
                return db.addTicket(this);
            }
            else
            {
                return -1;
            }
        }

        public void SetPerson(string fio, string passport)
        {
            FIO = fio;
            Passport = passport;
        }

        public void Print(string login)
        {
            PdfDocument document = new PdfDocument();
            PdfPage page = document.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);
            int y = 40;
            int x = 50;
            var img = Properties.Resources.main;
            MemoryStream ms = new MemoryStream();
            img.Save(ms, img.RawFormat);
            XImage image = XImage.FromStream(ms);
            ms.Close();
            gfx.DrawImage(image, x, y, 64, 64);
            gfx.DrawString("Maked by BSTO".ToString(), new XFont("Arial", 18), XBrushes.Black, new XRect(x + 74, y, page.Width - x - 74, 64), XStringFormats.CenterLeft);
            y += 90;
            gfx.DrawString("Билет №" + ticket_id.ToString(), new XFont("Arial", 26), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopCenter);
            y += 30;
            gfx.DrawString(route_id + ". " + db.getRouteStart(route_id) + '-' + db.getRouteEnd(route_id), new XFont("Arial", 20), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopCenter);
            y += 40;
            gfx.DrawString("Номер маршрута: " + cruise_id, new XFont("Arial", 16), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopLeft);
            y += 30;
            gfx.DrawString("Дата и время отправки: " + start_date, new XFont("Arial", 16), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopLeft);
            y += 30;
            gfx.DrawString("Начальная остановка: " + db.getRouteHaltAdress(start_halt_id), new XFont("Arial", 16), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopLeft);
            y += 30;
            gfx.DrawString("Конечная остановка: " + db.getRouteHaltAdress(end_halt_id), new XFont("Arial", 16), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopLeft);
            y += 30;
            gfx.DrawString("Водитель: " + driver_FIO, new XFont("Arial", 16), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopLeft);
            y += 30;
            gfx.DrawString("Номер ТС: " + transport_number, new XFont("Arial", 16), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopLeft);
            y += 60;
            gfx.DrawString("Номер места: " + place.ToString(), new XFont("Arial", 16), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopLeft);
            y += 30;
            gfx.DrawString("ФИО пассажира: " + FIO, new XFont("Arial", 16), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopLeft);
            y += 30;
            gfx.DrawString("Номер документа: " + Passport, new XFont("Arial", 16), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopLeft);
            y += 60;
            gfx.DrawString("Стоимость билета: " + Cost.ToString(), new XFont("Arial", 16), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopLeft);
            y += 30;
            gfx.DrawString("Дата и время покупки: " + selling_time.ToString(), new XFont("Arial", 16), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopLeft);
            y += 30;
            gfx.DrawString("Продавец: " + seller_FIO, new XFont("Arial", 16), XBrushes.Black, new XRect(x, y, page.Width - x, 18), XStringFormats.TopLeft);
            y += 30;
            var stimg = Properties.Resources.Stamp;
            MemoryStream stms = new MemoryStream();
            stimg.Save(stms, stimg.RawFormat);
            XImage stimage = XImage.FromStream(stms);
            stms.Close();
            gfx.DrawImage(stimage, page.Width - 150, y, 120, 120);
            document.Save(ticket_id.ToString() + ".pdf");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
