﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTO.Model
{
    public class TravellModel : INotifyPropertyChanged
    {
        public int cruise_id { get; set; }
        public int Places { get; set; }
        public int Cost { get; set; }
        public string Time { get; set; }
        public DateTime CruiseTime;
        public DateTime Date { get; set; }
        public int RouteId { get; set; }
        public string RouteStart { get; set; }
        public string RouteEnd { get; set; }


        public TravellModel()
        {

        }

        public TravellModel(CruiseModel cruise, int start, int end, DateTime date)
        {
            DBOperations db = new DBOperations();
            cruise_id = cruise.cruise_id;
            int startCost = db.getRouteHaltCostById(start);
            int endCost = db.getRouteHaltCostById(end);
            Cost = endCost - startCost;
            Date = date;
            CruiseTime = date;
            Time = Date.TimeOfDay.ToString().Substring(0, 5);
            Places = cruise.Transport.Places_count;
            Places -= db.getNotFreePlacesCount(cruise.cruise_id, date.Date, start, end);
            RouteId = cruise.route_id;
            RouteStart = db.getRouteStart(RouteId);
            RouteEnd = db.getRouteEnd(RouteId);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
