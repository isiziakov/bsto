﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTO.Model
{
    public class CruisesModel : INotifyPropertyChanged
    {
        public ObservableCollection<CruiseModel> cruises { get; set; }

        public bool[] daysMask;

        private string _daysStr;
        public string daysStr
        {
            get { return _daysStr; }
            set
            {
                _daysStr = value;
                OnPropertyChanged("daysStr");
            }
        }

        public CruisesModel(CruiseModel cruise)
        {
            daysStr = "";
            daysMask = new bool[7];
            cruises = new ObservableCollection<CruiseModel>();
            Add(cruise);
        }

        public void Add(CruiseModel cruise)
        {
            cruises.Add(cruise);
            daysMask[cruise.day - 1] = true;
            daysStr += DaysConverter.IntTiStringShort(cruise.day) + ' ';
        }

        public bool Comparer(CruiseModel cruise)
        {
            if (cruises.Count == 0)
            {
                return true;
            }
            else
            {
                CruiseModel buf = cruises.First();
                return buf.Time == cruise.Time && buf.transport_id == cruise.transport_id && buf.StartDate == cruise.StartDate && buf.Hidden == cruise.Hidden && buf.EndingDate == cruise.EndingDate && buf.driver_id == cruise.driver_id;
            }
        }

        public bool RouteComparer(CruiseModel cruise)
        {
            if (cruises.Count == 0)
            {
                return true;
            }
            else
            {
                CruiseModel buf = cruises.First();
                return buf.Time == cruise.Time;
            }
        }

        public CruiseModel Pop(int day)
        {
            CruiseModel result = cruises.Where(i => i.day == day).FirstOrDefault();
            if (result != null)
            {
                cruises.Remove(result);
                daysMask[day - 1] = false;
                daysStr = daysStr.Replace(DaysConverter.IntTiStringShort(day) + ' ', "");
            }
            return result;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
