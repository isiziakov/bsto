﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTO.Model
{
    public class DriverModel : INotifyPropertyChanged
    {
        public int driver_id { get; set; }
        private int hidden { get; set; }
        public int Hidden
        {
            get { return hidden; }
            set
            {
                hidden = value;
                OnPropertyChanged("Hidden");
            }
        }
        private string fio { get; set; }
        public string FIO
        {
            get { return fio; }
            set
            {
                fio = value;
                string[] buffer = fio.Split(' ');
                if (FIO != "")
                {
                    Family = buffer[0];
                    Name = buffer[1];
                    if (buffer.Length > 2)
                    {
                        Surname = buffer[2];
                    }
                    else
                    {
                        Surname = "";
                    }
                }
                OnPropertyChanged("FIO");
            }
        }

        private string family { get; set; }
        public string Family
        {
            get { return family; }
            set
            {
                family = value;
                OnPropertyChanged("Family");
            }
        }

        private string name { get; set; }
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        private string surname { get; set; }
        public string Surname
        {
            get { return surname; }
            set
            {
                surname = value;
                OnPropertyChanged("Surname");
            }
        }
        public DriverModel()
        {

        }

        public DriverModel(Driver driver)
        {
            driver_id = driver.driver_id;
            FIO = driver.FIO;
            hidden = driver.hidden;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
