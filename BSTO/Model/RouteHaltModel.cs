﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTO.Model
{
    public class RouteHaltModel : INotifyPropertyChanged
    {
        private int routeHaltId { get; set; }
        public int RouteHaltId
        {
            get { return routeHaltId; }
            set
            {
                routeHaltId = value;
                OnPropertyChanged("RouteHaltId");
            }
        }
        private int haltId { get; set; }
        public int HaltId
        {
            get { return haltId; }
            set
            {
                haltId = value;
                OnPropertyChanged("HaltId");
            }
        }

        private int routeId { get; set; }
        public int RouteId
        {
            get { return routeId; }
            set
            {
                routeId = value;
                OnPropertyChanged("RouteId");
            }
        }

        private int cost { get; set; }
        public int Cost
        {
            get { return cost; }
            set
            {
                cost = value;
                OnPropertyChanged("Cost");
            }
        }

        private int time { get; set; }
        public int Time
        {
            get { return time; }
            set
            {
                time = value;
                OnPropertyChanged("Time");
            }
        }

        private int hidden { get; set; }
        public int Hidden
        {
            get { return hidden; }
            set
            {
                hidden = value;
                OnPropertyChanged("Hidden");
            }
        }
        private int number { get; set; }
        public int Number
        {
            get { return number; }
            set
            {
                number = value;
                OnPropertyChanged("Number");
            }
        }

        private string locality { get; set; }
        public string Locality
        {
            get { return locality; }
            set
            {
                locality = value;
                OnPropertyChanged("Locality");
            }
        }
        private string adress { get; set; }
        public string Adress
        {
            get { return adress; }
            set
            {
                adress = value;
                OnPropertyChanged("Adress");
            }
        }

        public RouteHaltModel()
        {

        }

        public RouteHaltModel(RouteHalt halt)
        {
            routeHaltId = halt.route_halt_id;
            routeId = halt.route_id;
            haltId = halt.halt_id;
            cost = halt.cost;
            hidden = halt.hidden;
            number = halt.number_in_route;
            time = halt.time;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
