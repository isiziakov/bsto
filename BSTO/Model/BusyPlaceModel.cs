﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTO.Model
{
    public class BusyPlaceModel
    {
        public int cruiseId;
        public int place;
        public DateTime date;
        public int start_halt_id;
        public int end_halt_id;

        public BusyPlaceModel(int cruiseId, int place, DateTime date, int start_halt_id, int end_halt_id)
        {
            this.cruiseId = cruiseId;
            this.place = place;
            this.date = date;
            this.start_halt_id = start_halt_id;
            this.end_halt_id = end_halt_id;
        }
    }
}
