﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BSTO.Model
{
    public class DBOperations
    {
        public UserModel getUserByLogin(string login)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            User user = db.User.Where(i => i.login == login).FirstOrDefault();
            if (user != null)
            {
                return new UserModel(user);
            }
            else
            {
                return null;
            }
        }

        public string getUserFIOByLogin(string login)
        {
            UserModel user = getUserByLogin(login);
            if (user != null)
            {
                return user.FIO;
            }
            else
            {
                return "";
            }
        }

        public string getUserType(int type_id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            UserType type = db.UserType.Where(i => i.type_id == type_id).FirstOrDefault();
            if (type != null)
            {
                return type.type;
            }
            else
            {
                return null;
            }
        }

        public List<HaltModel> getHalts(bool hidden)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            List<Halt> halts;
            if (hidden == true)
            {
                halts = db.Halt.ToList();
            }
            else
            {
                halts = db.Halt.Where(i => i.hidden == 0).ToList();
            }
            if (halts != null)
            {
                return halts.Select(i => new HaltModel(i)).ToList();
            }
            else
            {
                return null;
            }
        }

        public List<HaltModel> getHaltsWithContext(bool hidden, string context)
        {
            List<HaltModel> result = getHalts(hidden);
            if (result != null)
            {
                result = result.Where(i => i.Adress.ToLower().Contains(context.ToLower())).ToList();
                if (result.Count > 1)
                {
                    result.Sort((e1, e2) =>
                    {
                        return e1.Hidden.CompareTo(e2.Hidden);
                    });
                }
            }
            return result;
        }

        public List<HaltModel> getHaltsByLocality(int locality)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            List<Halt> halts = db.Halt.Where(i => i.locality_id == locality).ToList();
            return halts.Select(i => new HaltModel(i)).ToList();
        }

        public int addHalt(HaltModel halt)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            Halt newHalt = new Halt();
            newHalt.adress = halt.Adress;
            newHalt.locality_id = halt.locality_id;
            newHalt.hidden = halt.Hidden;
            db.Halt.Add(newHalt);
            Save(db);
            return newHalt.halt_id;
        }

        public void updateHalt(HaltModel halt)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            Halt updatedHalt = db.Halt.Find(halt.halt_id);
            updatedHalt.adress = halt.Adress;
            updatedHalt.locality_id = halt.locality_id;
            updatedHalt.hidden = halt.Hidden;
            db.Entry(updatedHalt).State = System.Data.Entity.EntityState.Modified;
            Save(db);
        }

        public List<DriverModel> getDrivers(bool hidden)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            List<Driver> drivers;
            if (hidden == true)
            {
                drivers = db.Driver.ToList();
            }
            else
            {
                drivers = db.Driver.Where(i => i.hidden == 0).ToList();
            }
            if (drivers != null)
            {
                return drivers.Select(i => new DriverModel(i)).ToList();
            }
            else
            {
                return null;
            }
        }

        public List<DriverModel> getDriversWithContext(bool hidden, string context)
        {
            string[] words = null;
            List<DriverModel> result = getDrivers(hidden);
            if (result != null)
            {
                if (context != "")
                {
                    context = context.ToLower();
                    if (context[0] == ' ')
                    {
                        context = context.Remove(0, 1);
                    }
                    if (context != "")
                    {
                        if (context[context.Length - 1] == ' ')
                        {
                            context = context.Remove(context.Length -1, 1);
                        }
                        words = context.Split(' ');
                    }
                }
                if (words != null && words.Length > 0)
                {
                    foreach (string str in words)
                    {
                        result = result.Where(i => i.FIO.ToLower().Contains(str)).ToList();
                    }
                }
            }
            if (result.Count > 1)
            {
                result.Sort((e1, e2) =>
                {
                    return e1.Hidden.CompareTo(e2.Hidden);
                });
            }
            return result;
        }

        public int addDriver(DriverModel driver)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            Driver newDriver = new Driver();
            newDriver.FIO = driver.FIO;
            newDriver.hidden = driver.Hidden;
            db.Driver.Add(newDriver);
            Save(db);
            return newDriver.driver_id;
        }

        public void updateDriver(DriverModel driver)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            Driver updatedDriver = db.Driver.Find(driver.driver_id);
            updatedDriver.FIO = driver.FIO;
            updatedDriver.hidden = driver.Hidden;
            db.Entry(updatedDriver).State = System.Data.Entity.EntityState.Modified;
            Save(db);
        }

        public Driver getDriverById(int id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            return db.Driver.Where(i => i.driver_id == id).FirstOrDefault();
        }
        public DriverModel getDriver(int id)
        {
            return new DriverModel(getDriverById(id));
        }

        public List<CruiseModel> getCruisesForSearch(int start, int end)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            List<DAL.Entity.Route> routes = db.Route.Where(i => i.hidden == 0).ToList();
            List<CruiseModel> result = new List<CruiseModel>();
            foreach (DAL.Entity.Route r in routes)
            {
                RouteHaltModel st = getRouteHaltsByRouteId(false, r.route_id).Where(i => i.HaltId == start).FirstOrDefault();
                RouteHaltModel en = getRouteHaltsByRouteId(false, r.route_id).Where(i => i.HaltId == end).FirstOrDefault();
                if (st != null && en != null && st.Number < en.Number)
                {
                    result.AddRange(getCruisesForRoute(r.route_id, false));
                }
            }
            return result;
        }

        public List<TransportModel> getTransports(bool hidden)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            List<Transport> transports;
            if (hidden == true)
            {
                transports = db.Transport.Where(i => i.hidden < 2).ToList();
            }
            else
            {
                transports = db.Transport.Where(i => i.hidden == 0).ToList();
            }
            if (transports != null)
            {
                return transports.Select(i => new TransportModel(i)).ToList();
            }
            else
            {
                return null;
            }
        }

        public List<TransportModel> getTransportsWithContext(bool hidden, string context)
        {
            List<TransportModel> result = getTransports(hidden);
            if (result != null)
            {
                result = result.Where(i => i.Number.ToLower().Contains(context.ToLower())).ToList();
                if (result.Count > 1)
                {
                    result.Sort((e1, e2) =>
                    {
                        return e1.Hidden.CompareTo(e2.Hidden);
                    });
                }
            }
            return result;
        }

        public int addTransport(TransportModel transport)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            Transport newTransport = new Transport();
            newTransport.number = transport.Number;
            newTransport.places_count = transport.Places_count;
            newTransport.hidden = transport.Hidden;
            db.Transport.Add(newTransport);
            Save(db);
            return newTransport.transport_id;
        }

        public void updateTransport(TransportModel transport)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            Transport updatedTransport = db.Transport.Find(transport.transport_id);
            updatedTransport.number = transport.Number;
            updatedTransport.places_count = transport.Places_count;
            updatedTransport.hidden = transport.Hidden;
            db.Entry(updatedTransport).State = System.Data.Entity.EntityState.Modified;
            Save(db);
        }

        public Transport getTransportById(int id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            return db.Transport.Where(i => i.transport_id == id).FirstOrDefault();
        }

        public TransportModel getTransport(int id)
        {
            return new TransportModel(getTransportById(id));
        }

        public List<RouteModel> getRoutes(bool hidden)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            List<DAL.Entity.Route> routes;
            if (hidden == true)
            {
                routes = db.Route.ToList();
            }
            else
            {
                routes = db.Route.Where(i => i.hidden == 0).ToList();
            }
            if (routes != null)
            {
                return routes.Select(i => new RouteModel(i)).ToList();
            }
            else
            {
                return null;
            }
        }

        public List<RouteModel> getRoutesWithContext(bool hidden, string context)
        {
            List<RouteModel> result = getRoutes(hidden);
            if (result != null)
            {
                result = result.Where(i => i.Number.ToString().Contains(context.ToLower())).ToList();
                if (result.Count > 1)
                {
                    result.Sort((e1, e2) =>
                    {
                        return e1.Hidden.CompareTo(e2.Hidden);
                    });
                }
            }
            return result;
        }

        public int addRoute(RouteModel route)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            DAL.Entity.Route newRoute = new DAL.Entity.Route();
            newRoute.hidden = route.Hidden;
            db.Route.Add(newRoute);
            Save(db);
            return newRoute.route_id;
        }

        public void updateRoute(RouteModel route)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            DAL.Entity.Route updatedRoute = db.Route.Find(route.Number);
            updatedRoute.hidden = route.Hidden;
            db.Entry(updatedRoute).State = System.Data.Entity.EntityState.Modified;
            Save(db);
        }

        public string getRouteStart(int id)
        {
            List<RouteHaltModel> buffer = getRouteHaltsByRouteId(false, id);
            if (buffer.Count != 0)
            {
                return getLocality(getHaltById(buffer.First().HaltId).locality_id).locality_name;
            }
            else
            {
                return "";
            }
        }

        public string getRouteEnd(int id)
        {
            List<RouteHaltModel> buffer = getRouteHaltsByRouteId(false, id);
            if (buffer.Count != 0)
            {
                return getLocality(getHaltById(buffer.Last().HaltId).locality_id).locality_name;
            }
            else
            {
                return "";
            }
        }

        public List<CruiseModel> getCruisesForRoute(int route_id, bool hidden)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            List<DAL.Entity.Cruise> cruises = db.Cruise.Where(i => i.route_id == route_id).ToList();
            if (!hidden)
            {
                cruises = cruises.Where(i => i.hidden == 0).ToList();
            }
            else
            {
                cruises = cruises.Where(i => i.hidden <= 1).ToList();
            }
            if (cruises != null)
            {
                List<CruiseModel> result = cruises.Select(i => new CruiseModel(i)).ToList();
                result = result.OrderBy(x => x.day).ThenBy(x => x.Time).ToList();
                return result;
            }
            else
            {
                return null;
            }
        }
        public int addCruise(CruiseModel cruise)
        {
            DBContext db = new DBContext();
            checkDB(db);
            DAL.Entity.Cruise newCruise = new DAL.Entity.Cruise();
            newCruise.route_id = cruise.route_id;
            newCruise.time = cruise.Time;
            newCruise.day = cruise.day;
            if (DateTime.TryParse(cruise.EndingDate, out DateTime number))
            {
                newCruise.ending_date = DateTime.Parse(cruise.EndingDate);
            }
            else
            {
                newCruise.ending_date = null;
            }
            if (DateTime.TryParse(cruise.StartDate, out DateTime number2))
            {
                newCruise.start_date = DateTime.Parse(cruise.StartDate);
            }
            else
            {
                newCruise.start_date = null;
            }
            newCruise.driver_id = cruise.driver_id;
            newCruise.transport_id = cruise.transport_id;
            newCruise.hidden = cruise.Hidden;
            newCruise.first = cruise.first;
            db.Cruise.Add(newCruise);
            Save(db);
            return newCruise.cruise_id;
        }

        public void updateCruise(CruiseModel cruise)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            DAL.Entity.Cruise updatedCruise = db.Cruise.Find(cruise.cruise_id);
            if (updatedCruise != null)
            {
                updatedCruise.time = cruise.Time;
                if (DateTime.TryParse(cruise.EndingDate, out DateTime number))
                {
                    updatedCruise.ending_date = DateTime.Parse(cruise.EndingDate);
                }
                else
                {
                    updatedCruise.ending_date = null;
                }
                if (DateTime.TryParse(cruise.StartDate, out DateTime number2))
                {
                    updatedCruise.start_date = DateTime.Parse(cruise.StartDate);
                }
                else
                {
                    updatedCruise.start_date = null;
                }
                updatedCruise.day = cruise.day;
                updatedCruise.driver_id = cruise.driver_id;
                updatedCruise.transport_id = cruise.transport_id;
                updatedCruise.first = cruise.first;
                updatedCruise.hidden = cruise.Hidden;
                db.Entry(updatedCruise).State = System.Data.Entity.EntityState.Modified;
                Save(db);
            }
        }

        public void updateCruisesTransport(int oldTransportId, int newTransportId)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            List<DAL.Entity.Cruise> cruises = db.Cruise.Where(i => i.transport_id == oldTransportId).ToList();
            foreach (DAL.Entity.Cruise cruise in cruises)
            {
                cruise.transport_id = newTransportId;
                db.Entry(cruise).State = System.Data.Entity.EntityState.Modified;
            }
            Save(db); 
        }

        public List<RouteHaltModel> getRouteHaltsByRouteId(bool hidden, int routeId)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            List<RouteHalt> halts = db.RouteHalt.Where(i => i.route_id == routeId).ToList();
            if (hidden == false)
            {
                halts = halts.Where(i => i.hidden == 0).ToList();
            }
            if (halts.Count == 0)
            {
                return new List<RouteHaltModel>();
            }
            else
            {
                List<RouteHaltModel> result = halts.Where(i => i.hidden == 0).Select(i => new RouteHaltModel(i)).ToList();
                result.Sort((e1, e2) =>
                {
                    return e1.Number.CompareTo(e2.Number);
                });
                if (hidden)
                {
                    List<RouteHaltModel> buffer = halts.Where(i => i.hidden == 1).Select(i => new RouteHaltModel(i)).ToList();
                    buffer.Sort((e1, e2) =>
                    {
                        return e1.Number.CompareTo(e2.Number);
                    });
                    result.AddRange(buffer);
                }
                return result;
            }
        }
        public List<RouteHaltModel> getAllRouteHaltsByRouteId(int routeId)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            List<RouteHalt> halts = db.RouteHalt.Where(i => i.route_id == routeId).ToList();
            List<RouteHaltModel> result = halts.Where(i => i.hidden == 0).Select(i => new RouteHaltModel(i)).ToList();
            result.Sort((e1, e2) =>
            {
                return e1.Number.CompareTo(e2.Number);
            });
            return result;
        }

        public int addRouteHalt(RouteHaltModel halt)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            RouteHalt newHalt = new RouteHalt();
            newHalt.halt_id = halt.HaltId;
            newHalt.hidden = halt.Hidden;
            newHalt.number_in_route = halt.Number;
            newHalt.route_id = halt.RouteId;
            newHalt.cost = halt.Cost;
            newHalt.time = halt.Time;
            db.RouteHalt.Add(newHalt);
            Save(db);
            return newHalt.route_halt_id;
        }

        public void updateRouteHalt(RouteHaltModel halt)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            RouteHalt updatedHalt = db.RouteHalt.Find(halt.RouteHaltId);
            updatedHalt.halt_id = halt.HaltId;
            updatedHalt.hidden = halt.Hidden;
            updatedHalt.number_in_route = halt.Number;
            updatedHalt.time = halt.Time;
            db.Entry(updatedHalt).State = System.Data.Entity.EntityState.Modified;
            Save(db);
        }

        public List<RouteHaltModel> getAllRouteHalts()
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            return db.RouteHalt.ToList().Select(i => new RouteHaltModel(i)).ToList();
        }

        public int getRouteHaltHalt(int id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            return db.RouteHalt.Where(i => i.route_halt_id == id).First().halt_id;
        }
        public int getRouteHaltNumberById(int id, int routeId)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            RouteHalt r = db.RouteHalt.Where(i => i.route_halt_id == id && i.route_id == routeId).FirstOrDefault();
            if (r == null)
            {
                return -1;
            }
            else
            {
                return r.number_in_route;
            }
        }

        public int getRouteHaltNumberById(int id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            RouteHalt r = db.RouteHalt.Where(i => i.route_halt_id == id).FirstOrDefault();
            if (r == null)
            {
                return -1;
            }
            else
            {
                return r.number_in_route;
            }
        }

        public List<CruiseModel> getCruisesByHalts(int start, int end)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            return db.Cruise.ToList().Where(i => i.hidden == 0 && getRouteHaltNumberById(start, i.route_id) < getRouteHaltNumberById(end, i.route_id)).Select(i => new CruiseModel(i)).ToList();
        }

        public int getRouteHaltCostById(int id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            return db.RouteHalt.Where(i => i.route_halt_id == id).First().cost;
        }

        public int getRouteHaltTimeById(int id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            return db.RouteHalt.Where(i => i.route_halt_id == id).First().time;
        }

        public HaltModel getHaltById(int halt_id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            return new HaltModel(db.Halt.Where(i => i.halt_id == halt_id && i.hidden == 0).FirstOrDefault());
        }

        public int getNotFreePlacesCount(int cruiseId, DateTime date, int startHalt, int endHalt)
        {
            return getSellingTicketForCruiseCount(cruiseId, date, startHalt, endHalt) + getBusyPlacesCount(cruiseId, date, startHalt, endHalt);
        }

        public int getSellingTicketForCruiseCount(int cruiseId, DateTime date, int startHalt, int endHalt)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            return db.Ticket.ToList().Where(i => i.closed == false && i.cruise_id == cruiseId && i.date.Date == date.Date && getRouteHaltNumberById(i.start_halt_id, i.route_id) < getRouteHaltNumberById(endHalt, i.route_id) && getRouteHaltNumberById(i.end_halt_id, i.route_id) > getRouteHaltNumberById(startHalt, i.route_id) && !i.returned).Count();
        }

        public int getBusyPlacesCount(int cruiseId, DateTime date, int startHalt, int endHalt)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            return db.BusyPlaces.ToList().Where(i => i.cruise_id == cruiseId && i.date.Date == date.Date && getRouteHaltNumberById(i.start_halt_id) < getRouteHaltNumberById(endHalt) && getRouteHaltNumberById(i.end_halt_id) > getRouteHaltNumberById(startHalt)).Count();
        }

        public List<int> getFreePlaces(int cruiseId, DateTime date, int startHalt, int endHalt)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            List<Ticket> buffer = db.Ticket.ToList().Where(i => i.closed == false && i.cruise_id == cruiseId && i.date.Date == date.Date && getRouteHaltNumberById(i.start_halt_id, i.route_id) < getRouteHaltNumberById(endHalt, i.route_id) && getRouteHaltNumberById(i.end_halt_id, i.route_id) > getRouteHaltNumberById(startHalt, i.route_id) && !i.returned).ToList();
            List<BusyPlaces> busy = db.BusyPlaces.ToList().Where(i => i.cruise_id == cruiseId && i.date.Date == date.Date && getRouteHaltNumberById(i.start_halt_id) < getRouteHaltNumberById(endHalt) && getRouteHaltNumberById(i.end_halt_id) > getRouteHaltNumberById(startHalt)).ToList();
            List<int> result = new List<int>();
            int? places = getTransportForCruise(cruiseId).places_count;
            if (places != null)
            {
                for (int i = 1; i <= places.Value; i++)
                {
                    if (buffer.Where(j => j.place == i).FirstOrDefault() == null && busy.Where(j => j.place == i).FirstOrDefault() == null)
                    {
                        result.Add(i);
                    }
                }
            }
            return result;
        }

        public Transport getTransportForCruise(int cruiseId)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            DAL.Entity.Cruise cruise = db.Cruise.Where(i => i.cruise_id == cruiseId).FirstOrDefault();
            return db.Transport.Where(i => i.transport_id == cruise.transport_id).FirstOrDefault();
        }

        public void addBusyPlace(BusyPlaceModel place)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            BusyPlaces busy = new BusyPlaces();
            busy.cruise_id = place.cruiseId;
            busy.place = place.place;
            busy.date = place.date;
            busy.start_halt_id = place.start_halt_id;
            busy.end_halt_id = place.end_halt_id;
            db.BusyPlaces.Add(busy);
            Save(db);
        }

        public void deleteBusyPlace(BusyPlaceModel place)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            BusyPlaces busy = db.BusyPlaces.Where(i => i.cruise_id == place.cruiseId && i.place == place.place && i.date == place.date && i.start_halt_id == place.start_halt_id && i.end_halt_id == place.end_halt_id).FirstOrDefault();
            if (busy != null)
            {
                db.BusyPlaces.Remove(busy);
                Save(db);
            }
        }

        public CruiseModel getCruiseById(int id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            DAL.Entity.Cruise cr = db.Cruise.Where(i => i.cruise_id == id).FirstOrDefault();
            if (cr != null)
            {
                return new CruiseModel(cr);
            }
            else
            {
                return new CruiseModel();
            }
        }

        public int addTicket(TicketModel ticket)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            Ticket newTicket = new Ticket();
            newTicket.cruise_id = ticket.cruise_id;
            newTicket.date = ticket.date;
            newTicket.driver_id = ticket.driver_id;
            newTicket.end_halt_id = ticket.end_halt_id;
            newTicket.FIO = ticket.FIO;
            newTicket.passport = ticket.Passport;
            newTicket.place = ticket.Place;
            newTicket.returned = ticket.returned;
            newTicket.route_id = ticket.route_id;
            newTicket.seller_id = ticket.seller_id;
            newTicket.selling_time = ticket.selling_time;
            newTicket.start_date = ticket.start_date;
            newTicket.start_halt_id = ticket.start_halt_id;
            newTicket.transport_id = ticket.transport_id;
            newTicket.closed = ticket.closed;
            newTicket.rplace = ticket.rplace;
            newTicket.rtime = ticket.rtime;
            newTicket.cost = ticket.Cost;
            db.Ticket.Add(newTicket);
            if (Save(db) > 0)
            {
                return newTicket.ticket_id;
            }
            else 
            {
                return 0;
            }
        }

        public string getRouteHaltAdress(int id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            HaltModel halt = getHaltById(db.RouteHalt.Where(i => i.route_halt_id == id).FirstOrDefault().halt_id);
            return halt.Adress;
        }

        public string getRouteHaltAdressLocality(int id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            HaltModel halt = getHaltById(db.RouteHalt.Where(i => i.route_halt_id == id).FirstOrDefault().halt_id);
            return getLocality(halt.locality_id).locality_name + ", " + halt.Adress;
        }

        public LocalityModel getLocality(int id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            Localities l = db.Localities.Where(i => i.locality_id == id).FirstOrDefault();
            if (l == null)
            {
                return new LocalityModel();
            }
            else
            {
                return new LocalityModel(l);
            }
        }

        public List<LocalityModel> getLocalities()
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            return db.Localities.ToList().Select(i => new LocalityModel(i)).ToList();
        }

        public List<TicketModel> getTicketsForCruise(int id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            List<Ticket> tickets = db.Ticket.Where(i => i.cruise_id == id).ToList();
            return tickets.Select(i => new TicketModel(i)).ToList();
        }

        public List<TicketModel> getTicketsForRoute(int id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            List<Ticket> tickets = db.Ticket.Where(i => i.route_id == id).ToList();
            return tickets.Select(i => new TicketModel(i)).ToList();
        }

        public void updateTicket(TicketModel ticket)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            Ticket updatedTicket = new Ticket();
            updatedTicket.ticket_id = ticket.ticket_id;
            updatedTicket.cruise_id = ticket.cruise_id;
            updatedTicket.date = ticket.date;
            updatedTicket.driver_id = ticket.driver_id;
            updatedTicket.end_halt_id = ticket.end_halt_id;
            updatedTicket.FIO = ticket.FIO;
            updatedTicket.passport = ticket.Passport;
            updatedTicket.place = ticket.Place;
            updatedTicket.returned = ticket.returned;
            updatedTicket.route_id = ticket.route_id;
            updatedTicket.seller_id = ticket.seller_id;
            updatedTicket.selling_time = ticket.selling_time;
            updatedTicket.start_date = ticket.start_date;
            updatedTicket.start_halt_id = ticket.start_halt_id;
            updatedTicket.transport_id = ticket.transport_id;
            updatedTicket.cost = ticket.Cost;
            updatedTicket.closed = ticket.closed;
            updatedTicket.rplace = ticket.rplace;
            updatedTicket.rtime = ticket.rtime;
            db.Entry(updatedTicket).State = System.Data.Entity.EntityState.Modified;
            Save(db);
        }

        public TicketModel getTicketById(int id)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            Ticket ticket = db.Ticket.Where(i => i.ticket_id == id).FirstOrDefault();
            if (ticket == null)
            {
                return null;
            }
            else
            {
                return new TicketModel(ticket);
            }
        }
        public List<TicketModel> getTicketsByDate(DateTime date)
        {
            DBContext db = new DBContext(); 
            checkDB(db);
            var buffer = db.Ticket.Where(i => i.closed == false).ToList();
            buffer = buffer.Where(i => i.date.Date == date.Date).ToList();
            return buffer.Select(i => new TicketModel(i)).ToList();
        }

        public List<RouteModel> GetRoutesForDate(DateTime date)
        {
            DBContext db = new DBContext();
            checkDB(db);
            var buffer = db.Route.ToList();
            List<RouteModel> result = new List<RouteModel>();
            foreach (DAL.Entity.Route r in buffer)
            {
                List<CruiseModel> cruises = getCruisesForRoute(r.route_id, true);
                cruises = cruises.Where(i => i.first <= date.Date && (i.EndingDate == "" || DateTime.Parse(i.EndingDate) >= date.Date)).ToList();
                if (cruises.Count > 0)
                {
                    result.Add(new RouteModel(r));
                    result.Last().Cruises = new System.Collections.ObjectModel.ObservableCollection<CruiseModel>(cruises);
                }
            }
            return result;
        }

        private void DBException()
        {
            MessageBox.Show("Ошибка подключения к базе, приложение будет закрыто", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Warning);
            Environment.Exit(0);
        }

        private void checkDB(DBContext db)
        {
            try
            {
                if (!db.Database.Exists())
                {
                    DBException();
                }
            }
            catch (System.InvalidOperationException)
            {
                DBException();
            }
        }

        public int Save(DBContext db)
        {
            return db.SaveChanges();
        }
    }
}