﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTO.Model
{
    public class RouteModel : INotifyPropertyChanged
    {
        private int route_id;
        public int Number
        {
            get { return route_id; }
            set
            {
                route_id = value;
                OnPropertyChanged("Number");
            }
        }
        private int hidden;
        public int Hidden
        {
            get { return hidden; }
            set
            {
                hidden = value;
                OnPropertyChanged("Hidden");
            }
        }
        public string start { get; set; }
        
        public string ending { get; set; }

        private HaltModel startHalt = null;
        public HaltModel StartHalt
        {
            get { return startHalt; }
            set
            {
                startHalt = value;
                OnPropertyChanged("StartHalt");
            }
        }
        public ObservableCollection<CruiseModel> Cruises { get; set; }

        public ObservableCollection<CruisesModel> GroupCruises { get; set; }

        public RouteModel()
        {

        }

        public RouteModel(DAL.Entity.Route route)
        {
            DBOperations db = new DBOperations();
            route_id = route.route_id;
            hidden = route.hidden;
            Cruises = new ObservableCollection<CruiseModel>();
            start = db.getRouteStart(route_id);
            ending = db.getRouteEnd(route_id);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
