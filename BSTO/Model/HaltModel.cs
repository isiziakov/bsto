﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTO.Model
{
    public class HaltModel : INotifyPropertyChanged
    {
        public int halt_id { get; set; }
        private string adress;
        public string Adress
        {
            get { return adress; }
            set
            {
                adress = value;
                OnPropertyChanged("Adress");
            }
        }
        public int locality_id { get; set; }
        private LocalityModel locality_model { get; set; }
        public LocalityModel Locality_model
        {
            get { return locality_model; }
            set
            {
                locality_model = value;
                OnPropertyChanged("Locality_model");
            }
        }
        private int hidden { get; set; }
        public int Hidden
        {
            get { return hidden; }
            set
            {
                hidden = value;
                OnPropertyChanged("Hidden");
            }
        }

        public HaltModel()
        {

        }

        public HaltModel(Halt halt)
        {
            DBOperations db = new DBOperations();
            halt_id = halt.halt_id;
            adress = halt.adress;
            locality_id = halt.locality_id;
            hidden = halt.hidden;
            Locality_model = db.getLocality(locality_id);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
