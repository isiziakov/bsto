﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTO.Model
{
    public class LocalityModel
    {
        public int locality_id { get; set; }
        public string locality_name { get; set; }

        public LocalityModel()
        {

        }
        public LocalityModel(Localities locality)
        {
            locality_id = locality.locality_id;
            locality_name = locality.locality_name;
        }
    }

    

}
