﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTO.Model
{
    public static class DaysConverter
    {
        public static string IntTiString(int day)
        {
            switch (day)
            {
                case 1: return "понедельник";
                case 2: return "вторник"; 
                case 3: return "среда"; 
                case 4: return "четверг"; 
                case 5: return "пятница"; 
                case 6: return "суббота"; 
                case 7: return "воскресенье";
            }
            return "";
        }

        public static string IntTiStringShort(int day)
        {
            switch (day)
            {
                case 1: return "пн";
                case 2: return "вт";
                case 3: return "ср";
                case 4: return "чт";
                case 5: return "пт";
                case 6: return "сб";
                case 7: return "вс";
            }
            return "";
        }
        public static int EStringToInt(string day)
        {
            switch (day.ToLower())
            {
                case "monday": return 1;
                case "tuesday": return 2;
                case "wednesday": return 3;
                case "thursday": return 4;
                case "friday": return 5;
                case "saturday": return 6;
                case "sunday": return 7;
            }
            return -1;
        }
    }
}
