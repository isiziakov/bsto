﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTO.Model
{
    public class TransportModel : INotifyPropertyChanged
    {
        public int transport_id { get; set; }
        private string number { get; set; }
        public string Number
        {
            get { return number; }
            set
            {
                number = value;
                setNumberPlaces();
                OnPropertyChanged("Number");
            }
        }
        private int places_count { get; set; }
        public int Places_count
        {
            get { return places_count; }
            set
            {
                places_count = value;
                setNumberPlaces();
                OnPropertyChanged("Places_count");
            }
        }
        public string NumberPlaces { get; set; }

        private int hidden { get; set; }
        public int Hidden
        {
            get { return hidden; }
            set
            {
                hidden = value;
                OnPropertyChanged("Hidden");
            }
        }

        public TransportModel()
        {

        }

        public TransportModel(Transport transport)
        {
            transport_id = transport.transport_id;
            number = transport.number;
            places_count = transport.places_count;
            hidden = transport.hidden;
            setNumberPlaces();
        }

        private void setNumberPlaces()
        {
            NumberPlaces = number;
            if (places_count > 0)
            {
                NumberPlaces += " " + places_count;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
