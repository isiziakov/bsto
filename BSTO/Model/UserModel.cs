﻿using DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTO.Model
{
    public class UserModel
    {
        public int id { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public string FIO { get; set; }
        public int type_id { get; set; }
        public int hidden { get; set; }
        public UserModel()
        {

        }

        public UserModel(User user)
        {
            id = user.id;
            login = user.login;
            password = user.password;
            FIO = user.FIO;
            type_id = user.type_id;
            hidden = user.hidden;
        }
    }
}
