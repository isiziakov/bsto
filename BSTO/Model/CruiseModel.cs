﻿using System;
using DAL.Entity;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTO.Model
{
    public class CruiseModel : INotifyPropertyChanged
    {
        public int cruise_id { get; set; }
        private string time;
        public string Time
        {
            get { return time; }
            set
            {
                time = value;
                OnPropertyChanged("Time");
            }
        }

        private string endingDate;
        public string EndingDate
        {
            get { return endingDate; }
            set
            {
                endingDate = value;
                OnPropertyChanged("EndingDate");
            }
        }

        public DateTime first;

        private string startDate;
        public string StartDate
        {
            get { return startDate; }
            set
            {
                startDate = value;
                OnPropertyChanged("StartDate");
            }
        }

        private int hidden;
        public int Hidden
        {
            get { return hidden; }
            set
            {
                hidden = value;
                OnPropertyChanged("Hidden");
            }
        }

        public int route_id { get; set; }

        public int driver_id { get; set; }

        public int day { get; set; }
        private DriverModel driver = null;
        public DriverModel Driver
        {
            get { return driver; }
            set
            {
                driver = value;
                OnPropertyChanged("Driver");
            }
        }

        public int transport_id { get; set; }

        private TransportModel transport = null;
        public TransportModel Transport
        {
            get { return transport; }
            set
            {
                transport = value;
                OnPropertyChanged("Transport");
            }
        }

        public CruiseModel()
        {

        }

        public CruiseModel(DAL.Entity.Cruise cruise)
        {
            DBOperations db = new DBOperations();
            day = cruise.day;
            first = cruise.first;
            cruise_id = cruise.cruise_id;
            time = cruise.time;
            transport_id = cruise.transport_id;
            driver_id = cruise.driver_id;
            hidden = cruise.hidden;
            route_id = cruise.route_id;
            if (cruise.ending_date != null)
            {
                endingDate = cruise.ending_date.Value.Date.ToString();
                endingDate = endingDate.Replace(" 0:00:00", "");

            }
            else
            {
                endingDate = "";
            }
            if (cruise.start_date != null)
            {
                startDate = cruise.start_date.Value.Date.ToString();
                startDate = startDate.Replace(" 0:00:00", "");

            }
            else
            {
                startDate = "";
            }

            Transport tbuffer = db.getTransportById(transport_id);
            if (tbuffer != null)
            {
                transport = new TransportModel(tbuffer);
            }
            else
            {
                transport = new TransportModel { Number = "", transport_id = -1, Places_count = 0, Hidden = 0 };
            }



            Driver buffer = db.getDriverById(driver_id);
            if (buffer != null)
            {
                driver = new DriverModel(buffer);
            }
            else
            {
                driver = new DriverModel { Hidden = 0, FIO = "", driver_id = -1 };
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
