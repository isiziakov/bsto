namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RouteHalt")]
    public partial class RouteHalt
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RouteHalt()
        {
            Ticket = new HashSet<Ticket>();
            Ticket1 = new HashSet<Ticket>();
        }

        [Key]
        public int route_halt_id { get; set; }

        public int halt_id { get; set; }

        public int route_id { get; set; }

        public int cost { get; set; }

        public int hidden { get; set; }

        public int number_in_route { get; set; }

        public int time { get; set; }

        public virtual Halt Halt { get; set; }

        public virtual Route Route { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ticket> Ticket { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ticket> Ticket1 { get; set; }
    }
}
