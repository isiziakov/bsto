namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Localities
    {
        [Key]
        [Column(Order = 0)]
        public int locality_id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string locality_name { get; set; }
    }
}
