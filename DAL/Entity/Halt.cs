namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Halt")]
    public partial class Halt
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Halt()
        {
            RouteHalt = new HashSet<RouteHalt>();
        }

        [Key]
        public int halt_id { get; set; }

        [Required]
        [StringLength(50)]
        public string adress { get; set; }

        public int locality_id { get; set; }

        public int hidden { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RouteHalt> RouteHalt { get; set; }
    }
}
