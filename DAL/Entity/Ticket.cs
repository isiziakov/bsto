namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ticket")]
    public partial class Ticket
    {
        [Key]
        public int ticket_id { get; set; }

        public int seller_id { get; set; }

        public int route_id { get; set; }

        public int cruise_id { get; set; }

        public DateTime date { get; set; }

        public int transport_id { get; set; }

        public int driver_id { get; set; }

        public int place { get; set; }

        [Required]
        [StringLength(50)]
        public string FIO { get; set; }

        [Required]
        [StringLength(50)]
        public string passport { get; set; }

        public int start_halt_id { get; set; }

        public bool returned { get; set; }

        public int end_halt_id { get; set; }

        public DateTime selling_time { get; set; }

        public DateTime start_date { get; set; }

        public int cost { get; set; }

        public bool closed { get; set; }

        public bool rplace { get; set; }

        public bool rtime { get; set; }

        public virtual Cruise Cruise { get; set; }

        public virtual Driver Driver { get; set; }

        public virtual RouteHalt RouteHalt { get; set; }

        public virtual RouteHalt RouteHalt1 { get; set; }

        public virtual Transport Transport { get; set; }

        public virtual User User { get; set; }
    }
}
