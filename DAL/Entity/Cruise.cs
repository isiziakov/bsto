namespace DAL.Entity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Cruise")]
    public partial class Cruise
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cruise()
        {
            BusyPlaces = new HashSet<BusyPlaces>();
            Ticket = new HashSet<Ticket>();
        }

        [Key]
        public int cruise_id { get; set; }

        [Required]
        [StringLength(50)]
        public string time { get; set; }

        public int hidden { get; set; }

        public int route_id { get; set; }

        public int driver_id { get; set; }

        public int transport_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ending_date { get; set; }

        [Column(TypeName = "date")]
        public DateTime? start_date { get; set; }

        public int day { get; set; }

        [Column(TypeName = "date")]
        public DateTime first { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BusyPlaces> BusyPlaces { get; set; }

        public virtual Driver Driver { get; set; }

        public virtual Route Route { get; set; }

        public virtual Transport Transport { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ticket> Ticket { get; set; }
    }
}
