using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace DAL.Entity
{
    public partial class DBContext : DbContext
    {
        public DBContext()
            : base("name=DBContext")
        {
        }

        public virtual DbSet<BusyPlaces> BusyPlaces { get; set; }
        public virtual DbSet<Cruise> Cruise { get; set; }
        public virtual DbSet<Driver> Driver { get; set; }
        public virtual DbSet<Halt> Halt { get; set; }
        public virtual DbSet<Route> Route { get; set; }
        public virtual DbSet<RouteHalt> RouteHalt { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<Ticket> Ticket { get; set; }
        public virtual DbSet<Transport> Transport { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserType> UserType { get; set; }
        public virtual DbSet<Localities> Localities { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cruise>()
                .Property(e => e.time)
                .IsUnicode(false);

            modelBuilder.Entity<Cruise>()
                .HasMany(e => e.BusyPlaces)
                .WithRequired(e => e.Cruise)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cruise>()
                .HasMany(e => e.Ticket)
                .WithRequired(e => e.Cruise)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Driver>()
                .Property(e => e.FIO)
                .IsUnicode(false);

            modelBuilder.Entity<Driver>()
                .HasMany(e => e.Cruise)
                .WithRequired(e => e.Driver)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Driver>()
                .HasMany(e => e.Ticket)
                .WithRequired(e => e.Driver)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Halt>()
                .Property(e => e.adress)
                .IsUnicode(false);

            modelBuilder.Entity<Halt>()
                .HasMany(e => e.RouteHalt)
                .WithRequired(e => e.Halt)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Route>()
                .HasMany(e => e.Cruise)
                .WithRequired(e => e.Route)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Route>()
                .HasMany(e => e.RouteHalt)
                .WithRequired(e => e.Route)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RouteHalt>()
                .HasMany(e => e.Ticket)
                .WithRequired(e => e.RouteHalt)
                .HasForeignKey(e => e.start_halt_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RouteHalt>()
                .HasMany(e => e.Ticket1)
                .WithRequired(e => e.RouteHalt1)
                .HasForeignKey(e => e.end_halt_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ticket>()
                .Property(e => e.FIO)
                .IsUnicode(false);

            modelBuilder.Entity<Ticket>()
                .Property(e => e.passport)
                .IsUnicode(false);

            modelBuilder.Entity<Transport>()
                .Property(e => e.number)
                .IsUnicode(false);

            modelBuilder.Entity<Transport>()
                .HasMany(e => e.Cruise)
                .WithRequired(e => e.Transport)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Transport>()
                .HasMany(e => e.Ticket)
                .WithRequired(e => e.Transport)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .Property(e => e.login)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.FIO)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Ticket)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.seller_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserType>()
                .Property(e => e.type)
                .IsUnicode(false);

            modelBuilder.Entity<UserType>()
                .HasMany(e => e.User)
                .WithRequired(e => e.UserType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Localities>()
                .Property(e => e.locality_name)
                .IsUnicode(false);
        }
    }
}
